<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRevenueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revenue', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('thang');
            $table->integer('doanh_thu');
            $table->integer('dien');
            $table->integer('nuoc');
            $table->integer('mang');
            $table->integer('luong_nv');
            $table->integer('mat_bang');
            $table->integer('chi_khac');
            $table->integer('lai');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revenue');
    }
}
