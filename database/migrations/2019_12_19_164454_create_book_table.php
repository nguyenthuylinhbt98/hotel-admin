<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',191)->nullable();
            $table->string('phonenumber',191)->nullable();
            $table->string('email',191)->nullable();

            $table->string('name_room', 191)->nullable();
            $table->string('room',191)->nullable();

            $table->integer('nguoi_lon')->nullable();
            $table->integer('tre_em')->nullable();
            
            $table->string('check_in',191)->nullable();
            $table->string('check_out',191)->nullable();

            $table->integer('status')->default(0);
            $table->string('sum',191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book');
    }
}
