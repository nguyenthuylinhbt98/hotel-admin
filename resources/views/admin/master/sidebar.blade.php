<div class="app-sidebar colored">
    <div class="sidebar-header">
        <a class="header-brand" href="index.html">
            <div class="logo-img">
               <img style="width: 30px; height: 30px; border-radius: 50%" src="/avatar/{{Auth::user()->img}}" alt="lavalite"> 
            </div>
            <span class="text">ĐATN_20191</span>
        </a>
        <button type="button" class="nav-toggle"><i data-toggle="expanded" class="ik ik-toggle-right toggle-icon"></i></button>
        <button id="sidebarClose" class="nav-close"><i class="ik ik-x"></i></button>
    </div>
    
    <div class="sidebar-content">
        <div class="nav-container">
            <nav id="main-menu-navigation" class="navigation-main">
               
                <div class="nav-item active">
                    <a href="/admin/"><i class="ik ik-home"></i><span>Trang chủ</span></a>
                </div>
                <div class="nav-item">
                    <a href="/admin/user"><i class="ik ik-user"></i><span>Thành viên</span> </a>
                </div>
                <div class="nav-item has-sub">
                    <a href="javascript:void(0)"><i class="ik ik-layers"></i><span>Phòng khách sạn</span></a>
                    <div class="submenu-content">
                        <a href="/admin/room/danhsach" class="menu-item">Danh sách phòng</a>
                        <a href="/admin/room/chi-tiet" class="menu-item">Chi tiết phòng</a>
                        
                    </div>
                </div>              
                
                <div class="nav-item">
                    <a href="/admin/dat-phong"><i class="ik ik-award"></i><span>Đặt phòng</span> </a>
                </div>

                <div class="nav-item">
                    <a href="/admin/bai-viet"><i class="ik ik-book"></i><span>Bài viết</span> </a>
                </div>

                 <div class="nav-item ">
                    <a href="/admin/doanh-thu"><i class="ik ik-bar-chart-2"></i><span>Doanh thu</span></a>
                </div>
               

            </nav>
        </div>
    </div>
</div>