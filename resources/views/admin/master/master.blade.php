<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Quản lý |@yield('title')</title>
        {{-- <base href="{{ asset('/admin_assets') }}/"> --}}

        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />

        <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800" rel="stylesheet">

        @yield('css')
        
        
    </head>

    <body style="font-size: 18px;">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="wrapper">

            {{-- header --}}
            @include('admin.master.header')
            {{-- end header --}}
            <div class="page-wrap">
                {{-- sidebar --}}
                
                @include('admin.master.sidebar')
                {{-- end sidebar --}}
                @yield('content')
                

                <!-- footer -->
                @include('admin.master.footer')
                <!-- end footer -->
            </div>
        </div>
        
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->

        @yield('script')
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
