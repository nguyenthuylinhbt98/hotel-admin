<footer class="footer">
    <div class="w-100 clearfix">
        <span class="text-center text-sm-left d-md-inline-block"> Đại học Bách Khoa Hà Nội - Viện Điện tử-Viễn thông.</span>
        <span class="float-none float-sm-right mt-1 mt-sm-0 text-center">Đồ án tốt nghiệp <i style="padding: 10px;" class="fa fa-book text-danger"></i> <a href="http://lavalite.org/" class="text-dark" target="_blank"> Phạm Văn Bằng</a></span>
    </div>
</footer>