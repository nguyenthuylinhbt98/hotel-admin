@extends('admin.master.master')
@section('title', ' Bài viết')

@section('css')

    <link rel="stylesheet" href="/admin_assets/plugins/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/ionicons/dist/css/ionicons.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/icon-kit/dist/css/iconkit.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css">
    <link rel="stylesheet" href="/admin_assets/plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" href="/admin_assets/dist/css/theme.min.css">
    <script src="/admin_assets/src/js/vendor/modernizr-2.8.3.min.js"></script>
@endsection


   

@section('content')
    @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif
    <div class="main-content">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="ik ik-calendar bg-blue"></i>
                            <div class="d-inline">
                                <h5>Bài viết</h5>
                                <span>Danh sách bài viết</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <nav class="breadcrumb-container" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="../index.html"><i class="ik ik-home"></i></a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="#">Quản lí</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Danh sách bài viết</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <div align="center">
                <h2 style="margin: 40px;"><b>Danh sách bài viết</b></h2>

                
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">

                    <div class="panel panel-primary">

                        <div class="panel-body">
                            <div class="bootstrap-table">
                                <div class="table-responsive">
                                    @if(session('thongbao'))
                                       <div class="alert alert-success">
                                            {{ session('thongbao') }}
                                        </div>

                                    @endif
                                    <a href="/admin/bai-viet/add" class="btn btn-primary">Thêm bài viết</a>
                                    <table class="table table-bordered" style="margin-top:20px; text-align: center;">

                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Tiêu đề bài viết</th>
                                                <th>Ảnh minh họa</th>
                                                <th>Mô tả ngắn</th>
                                                <th width='18%'>Tùy chọn</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($blog as $item)
                                                <tr>
                                                    <th>{{$item->id}}</th>
                                                    <th>{{$item->name}}</th>
                                                    <th>
                                                        <img style="width: 350px; height: 200px;" src="/baiviet/{{$item->img}}" alt="">
                                                    </th>
                                                    <th>{{$item->mo_ta_ngan}}</th>
                                                    
                                                    <td>
                                                        <a href="/admin/bai-viet/edit/{{$item->id}}" class="btn btn-primary"><i class="fa fa-book" aria-hidden="true"></i> Sửa</a>
                                                        <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</a>
                                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Xóa bài viết</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="/admin/bai-viet/del/{{$item->id}}" method="get" accept-charset="utf-8">
                                                                        <div class="modal-body">
                                                                            @csrf
                                                                            <h4>Bạn có chắc chắn muốn xóa bài viết này</h4>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                                                                            <button type="submit" class="btn btn-danger">Xóa</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            
                                    
                                        </tbody>
                                    </table>
                                    {{-- <div align='right'>
                                        <ul class="pagination">
                                            {{$users->links()}}

                                        </ul>
                                    </div> --}}
                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </div>
                    </div>
                    <!--/.row-->


                </div>
                    
            </div>
        </div>
       
    </div>
    

@endsection


@section('script')

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="../src/js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
    <script src="/admin_assets/plugins/popper.js/dist/umd/popper.min.js"></script>
    <script src="/admin_assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/admin_assets/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
    <script src="/admin_assets/plugins/screenfull/dist/screenfull.js"></script>
    <script src="/admin_assets/plugins/moment/moment.js"></script>
    <script src="/admin_assets/plugins/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="/admin_assets/plugins/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js"></script>
    <script src="/admin_assets/dist/js/theme.min.js"></script>
    <script src="/admin_assets/js/calendar.js"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    
@endsection