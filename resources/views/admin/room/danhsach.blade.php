@extends('admin.master.master')
@section('title', ' Danh sách')

@section('css')

    <link rel="stylesheet" href="/admin_assets/plugins/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/ionicons/dist/css/ionicons.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/icon-kit/dist/css/iconkit.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css">
    <link rel="stylesheet" href="/admin_assets/plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" href="/admin_assets/dist/css/theme.min.css">
    <script src="/admin_assets/src/js/vendor/modernizr-2.8.3.min.js"></script>
@endsection


   

@section('content')
    @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif
    <div class="main-content">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="ik ik-calendar bg-blue"></i>
                            <div class="d-inline">
                                <h5>Danh sách phòng</h5>
                                <span>Sơ đồ đặt phòng</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <nav class="breadcrumb-container" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="../index.html"><i class="ik ik-home"></i></a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="#">Quản lí</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Danh sách phòng</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <div align="center">
                <h2 style="margin: 40px;"><b>Danh sách phòng khách sạn</b></h2>

                
            </div>
            <div class="row">
                <div class="col-md-1">
                     
                </div> 
                <div class="col-md-10">
                    @for($i=100; $i<=900; $i+=100)
                        @for($j=1; $j<=10; $j++ )
                            @php
                                $a = $i+$j
                            @endphp
                            @if(in_array($a, $dem))
                                <a href="/admin/room/danhsach/{{$a}}" title=""><div style="width: 85px; height: 50px; background-color: #e24c61; float: left; border: 1px; margin: 3px; border-radius: 5px; line-height: 50px; text-align: center; margin-bottom: 10px;">{{$i+$j}}</div></a>
                            @else
                                <a href="/admin/dat-phong/add" title=""><div style="width: 85px; height: 50px; background-color: #ffddcc; float: left; border: 1px; margin: 3px; border-radius: 5px; line-height: 50px; text-align: center; margin-bottom: 10px;">{{$i+$j}}</div></a>
                            @endif    
                        @endfor
                        <div style="clear: both;">
                                
                            </div>    
                    @endfor
                </div>
                <div class="col-md-1">
                     
                </div>  
                    
            </div>
        </div>
       
    </div>
    

@endsection


@section('script')

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="../src/js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
    <script src="/admin_assets/plugins/popper.js/dist/umd/popper.min.js"></script>
    <script src="/admin_assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/admin_assets/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
    <script src="/admin_assets/plugins/screenfull/dist/screenfull.js"></script>
    <script src="/admin_assets/plugins/moment/moment.js"></script>
    <script src="/admin_assets/plugins/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="/admin_assets/plugins/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js"></script>
    <script src="/admin_assets/dist/js/theme.min.js"></script>
    <script src="/admin_assets/js/calendar.js"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    
@endsection