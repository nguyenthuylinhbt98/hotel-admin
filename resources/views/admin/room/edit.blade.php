@extends('admin.master.master')
@section('title', ' Chi tiết phòng')

@section('css')

    <link rel="stylesheet" href="/admin_assets/plugins/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/ionicons/dist/css/ionicons.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/icon-kit/dist/css/iconkit.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css">
    <link rel="stylesheet" href="/admin_assets/plugins/summernote/dist/summernote-bs4.css">
    <link rel="stylesheet" href="/admin_assets/dist/css/theme.css">
    <script src="/admin_assets/src/js/vendor/modernizr-2.8.3.min.js"></script>
@endsection


   

@section('content')
    @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif

    <form action="" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        @csrf
        <div class="main-content">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="ik ik-calendar bg-blue"></i>
                            <div class="d-inline">
                                <h5>Chi tiết phòng</h5>
                                <span>Cập nhật chi tiết phòng</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <nav class="breadcrumb-container" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="../index.html"><i class="ik ik-home"></i></a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="#">Quản lí</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Cập nhật chi tiết</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            
            <div class="row" >
                    <div class="col-md-4">
                        <div class="card" style="min-height: 422px;">
                            <div class="card-header"><h3>Ảnh mô tả</h3></div>
                           <input id="for_img" type="file" name="img" class="form-control hidden" onchange="changeImg(this)">
                            <div class="avatar_img">
                               
                                <label for="for_img"> <img style="width: 386px; height: 300px" id="avatar" src="/mota/{{$room->img}}"></label> 
                               
                            </div>
                        </div>

                        <div style=" text-align: center;">
                            <button class="btn btn-success"  type="submit">Cập nhật mô tả</button>
                            <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                            
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col-lg-12 col-md-12">
                                        <div id="visitfromworld" >
                                            <div class="form-group">
                                                <b>Số phòng</b>
                                                <input value="{{$room->so_phong}}" type="text" name="so_phong" class="form-control">
                                            </div>
                                            
                                            <div class="form-group">
                                                <b>Mô tả ngắn</b>
                                                <textarea name="mo_ta_ngan" class="form-control" style="height: 80px;">{{$room->mo_ta_ngan}}</textarea>
                                               
                                            </div>
                                            
                                            <div class="form-group">
                                                <b>Giá phòng</b>
                                                <input value="{{$room->gia_phong}}" type="text" name="gia_phong" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <b>Mô tả chi tiết</b>
                                                
                                                <textarea class="form-control html-editor" name="mo_ta_chi_tiet">
                                                    {!!$room->mo_ta_chi_tiet!!}
                                                </textarea>
                                            </div>
                                           
                                           
                                            
                                          
                                           

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
            </div>
        </div>
       
    </div>
    </form>
    

@endsection


@section('script')

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="../src/js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
    <script src="/admin_assets/plugins/popper.js/dist/umd/popper.min.js"></script>
    <script src="/admin_assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/admin_assets/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
    <script src="/admin_assets/plugins/screenfull/dist/screenfull.js"></script>
    <script src="/admin_assets/plugins/sweetalert/dist/sweetalert.min.js"></script>
    <script src="/admin_assets/plugins/summernote/dist/summernote-bs4.min.js"></script>
    <script src="/admin_assets/dist/js/theme.min.js"></script>
    <script src="/admin_assets/js/layouts.js"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->

     <script>
          function changeImg(input){
                //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
                if(input.files && input.files[0]){
                    var reader = new FileReader();
                    //Sự kiện file đã được load vào website
                    reader.onload = function(e){
                        //Thay đổi đường dẫn ảnh
                        $('#avatar').attr('src',e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $(document).ready(function() {
                $('#avatar').click(function(){
                    $('#for_img').click();
                });
            });
    </script>
    
@endsection