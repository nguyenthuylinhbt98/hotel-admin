@extends('admin.master.master')
@section('title', ' Danh sách phòng')

@section('css')

    <link rel="stylesheet" href="/admin_assets/plugins/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/ionicons/dist/css/ionicons.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/icon-kit/dist/css/iconkit.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css">
    <link rel="stylesheet" href="/admin_assets/plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" href="/admin_assets/dist/css/theme.min.css">
    <script src="/admin_assets/src/js/vendor/modernizr-2.8.3.min.js"></script>
@endsection


   

@section('content')
    @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif

    <div class="main-content">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="ik ik-file-text bg-blue"></i>
                            <div class="d-inline">
                                <h5>Danh sách phòng</h5>
                              
                                <span>Chi tiết phòng </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <nav class="breadcrumb-container" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="../index.html"><i class="ik ik-home"></i></a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="#">Quản lý</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Chi tiết phòng</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-5">
                    <div class="card">
                        @if(empty($room))
                           <p style="padding: 10px; height: 500px; text-align: center; padding-top: 200px">Hiện chưa cập nhật thông tin chi tiết về phòng</p>
                        @else
                            {{-- @dd($room[0]) --}}
                            <div class="card-body">
                                <div class="text-center"> 
                                    <img src="../img/user.jpg" class="rounded-circle" width="150" />
                                    
                                    <p class="card-subtitle">Phòng {{$room[0]['so_phong']}}</p>
                                    {{-- <div class="row text-center justify-content-md-center">
                                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="ik ik-user"></i> <font class="font-medium">254</font></a></div>
                                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="ik ik-image"></i> <font class="font-medium">54</font></a></div>
                                    </div> --}}
                                </div>
                            </div>
                            <hr class="mb-0"> 
                            <div class="card-body"> 
                                <small class="text-muted d-block">Giá phòng </small>
                                <h6>{{ number_format($room[0]['gia_phong'], '0', '', '.') }}VNĐ/đêm</h6> 
                                <small class="text-muted d-block pt-10">Mô tả chi tiết</small>
                                <h6>{!!$room[0]['mo_ta_chi_tiet']!!}</h6> 
                                
                                
                                <br/>
                                <button class="btn btn-icon btn-facebook"><i class="fab fa-facebook-f"></i></button>
                                <button class="btn btn-icon btn-twitter"><i class="fab fa-twitter"></i></button>
                                <button class="btn btn-icon btn-instagram"><i class="fab fa-instagram"></i></button>
                            </div>

                        @endif
                       
                    </div>
                </div>
                <div class="col-lg-8 col-md-7">
                    <div class="card container" style="height: 500px;">
                        <h4 style="text-align:  center; margin-top: 20px;"> Danh sách đặt phòng</h4>
                        <table class="table table-bordered" style="margin-top:20px; text-align: center;">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Khách hàng</th>
                                    <th>Số điện thoại</th>
                                    <th>Check in</th>
                                    <th>Check out</th>
                                    <th width='13%'>Tùy chọn</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($book as $item)
                                    <tr>
                                        <th>{{$item->id}}</th>
                                        <th>{{$item->name}}</th>
                                        <th>{{$item->phonenumber}}</th>
                                        
                                        <th>{{$item->check_in}}</th>
                                        <th>{{$item->check_out}}</th>
                                        <td>
                                            <a href="/admin/dat-phong/edit/{{$item->id}}" class="btn btn-primary"><i class="fa fa-book" aria-hidden="true"></i> Chi tiết</a>
                                            
                                        </td>
                                    </tr>
                                @endforeach   
                        
                            </tbody>
                        </table>
                            
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    

@endsection


@section('script')

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="../src/js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
    <script src="/admin_assets/plugins/popper.js/dist/umd/popper.min.js"></script>
    <script src="/admin_assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/admin_assets/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
    <script src="/admin_assets/plugins/screenfull/dist/screenfull.js"></script>
    <script src="/admin_assets/plugins/moment/moment.js"></script>
    <script src="/admin_assets/plugins/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="/admin_assets/plugins/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js"></script>
    <script src="/admin_assets/dist/js/theme.min.js"></script>
    <script src="/admin_assets/js/calendar.js"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    
@endsection