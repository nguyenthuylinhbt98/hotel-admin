@extends('admin.master.master')
@section('title', ' User')

@section('css')

    <link rel="stylesheet" href="/admin_assets/plugins/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/ionicons/dist/css/ionicons.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/icon-kit/dist/css/iconkit.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css">
    <link rel="stylesheet" href="/admin_assets/plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" href="/admin_assets/dist/css/theme.min.css">
    <script src="/admin_assets/src/js/vendor/modernizr-2.8.3.min.js"></script>
@endsection


   

@section('content')


    <div class="main-content">

        <form action="/admin/user/add" method="post" accept-charset="utf-8" enctype="multipart/form-data">
            
            @csrf
            <div class="container-fluid">
                <div class="page-header">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="ik ik-calendar bg-blue"></i>
                            <div class="d-inline">
                                <h5>Thành viên</h5>
                                <span>Danh sách thành viên</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <nav class="breadcrumb-container" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="../index.html"><i class="ik ik-home"></i></a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="#">Quản lí</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Thành viên</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col-lg-8 col-md-12">
                                        <h3 class="card-title">Thêm thành viên</h3>
                                        <div id="visitfromworld" >
                                            <div class="form-group">
                                                <b>Họ tên</b>
                                                <input required value="{{old('full')}}" type="text" name="full" class="form-control">
                                            </div>
                                            
                                            <div class="form-group">
                                                <b>Email</b>
                                                <input required value="{{old('email')}}" type="email" name="email" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <b>Mật khẩu</b>
                                                <input required value="{{old('password')}}" type="password" name="password" class="form-control">
                                            </div>
                                           
                                            <div class="form-group">
                                                <b>Địa chỉ</b>
                                                <input required value="{{old('address')}}" type="text" name="address" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <b>Số điện thoại</b>
                                                <input required value="{{old('phone')}}" type="number" name="phone" class="form-control">
                                            </div>
                                          
                                            <div class="form-group">
                                                <b>Chức vụ</b>
                                                <select name="level" class="form-control">
                                                    <option value="0">Chức vụ</option>
                                                    <option value="1">Quản lí</option>
                                                    <option value="2">Tiếp tân</option>
                                                    <option value="3">Nhân viên</option>
                                                    <option value="4">Kế toán</option>
                                                </select>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card" style="min-height: 422px;">
                            <div class="card-header" ><h3>Ảnh đại diện</h3></div>
                           <input id="for_img" type="file" name="img" class="form-control hidden" onchange="changeImg(this)">
                            <div class="avatar_img">
                               
                                <label for="for_img"> <img style="width: 386px; height: 380px" id="avatar" src="http://www.ti.com/content/dam/ticom/images/icons/illustrative-icons/sensing/capacitive-touch-sensing-icon.png"></label> 
                               
                            </div>
                        </div>

                        <div style=" text-align: center;">
                            <button class="btn btn-success"  type="submit">Thêm thành viên</button>
                            <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                            
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    

@endsection


@section('script')

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="../src/js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
    <script src="/admin_assets/plugins/popper.js/dist/umd/popper.min.js"></script>
    <script src="/admin_assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/admin_assets/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
    <script src="/admin_assets/plugins/screenfull/dist/screenfull.js"></script>
    <script src="/admin_assets/plugins/moment/moment.js"></script>
    <script src="/admin_assets/plugins/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="/admin_assets/plugins/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js"></script>
    <script src="/admin_assets/dist/js/theme.min.js"></script>
    <script src="/admin_assets/js/calendar.js"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->

    <script>
          function changeImg(input){
                //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
                if(input.files && input.files[0]){
                    var reader = new FileReader();
                    //Sự kiện file đã được load vào website
                    reader.onload = function(e){
                        //Thay đổi đường dẫn ảnh
                        $('#avatar').attr('src',e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $(document).ready(function() {
                $('#avatar').click(function(){
                    $('#for_img').click();
                });
            });
    </script>
    
@endsection