@extends('admin.master.master')
@section('title', ' User')

@section('css')

    <link rel="stylesheet" href="/admin_assets/plugins/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/ionicons/dist/css/ionicons.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/icon-kit/dist/css/iconkit.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css">
    <link rel="stylesheet" href="/admin_assets/plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" href="/admin_assets/dist/css/theme.min.css">
    <script src="/admin_assets/src/js/vendor/modernizr-2.8.3.min.js"></script>
@endsection


   

@section('content')


    <div class="main-content">
                    <div class="container-fluid">

                        <div class="page-header">
              
            </div>
                        <div class="page-header">
                            <div class="row align-items-end">
                                <div class="col-lg-8">
                                    <div class="page-header-title">
                                        <i class="ik ik-file-text bg-blue"></i>
                                        <div class="d-inline">
                                            <h5>Thông tin chi tiết</h5>
                                            <span>{{$user->full}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="../index.html"><i class="ik ik-home"></i></a>
                                            </li>
                                            <li class="breadcrumb-item">
                                                <a href="#">Quản lý</a>
                                            </li>
                                            <li class="breadcrumb-item active" aria-current="page">Thông tin</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-5">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="text-center"> 
                                            <img src="/avatar/{{$user->img}}" class="rounded-circle" width="150" />
                                            <h4 class="card-title mt-10">{{$user->name}}</h4>
                                            <p class="card-subtitle">
                                                
                                                @if($user->level==1) Quản lí @endif
                                                @if($user->level==2) Tiếp tân @endif
                                                @if($user->level==3) Nhân viên @endif
                                                @if($user->level==4) Kế toán @endif
                                            </p>
                                            <div class="row text-center justify-content-md-center">
                                              
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="mb-0"> 
                                    <div class="card-body"> 
                                        <small class="text-muted d-block">Email </small>
                                        <h6>{{$user->email}}</h6> 
                                        <small class="text-muted d-block pt-10">Số điện thoại</small>
                                        <h6>{{$user->phone}}</h6> 
                                        <small class="text-muted d-block pt-10">Địa chỉ</small>
                                        <h6>{{$user->address}}</h6>
                                        
                                       
                                        <br/>
                                        <button class="btn btn-icon btn-facebook"><i class="fab fa-facebook-f"></i></button>
                                        <button class="btn btn-icon btn-twitter"><i class="fab fa-twitter"></i></button>
                                        <button class="btn btn-icon btn-instagram"><i class="fab fa-instagram"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-7">
                                <div class="card">
                                    @if(session('thongbao'))
                                       <div class="alert alert-success">
                                            {{ session('thongbao') }}
                                        </div>

                                    @endif
                                    <form action="/admin/user/edit/{{$user->id}}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                                        @csrf
                                        <div class="container" style="font-size: 15px;">
                                            <h2 style="margin-top: 40px; text-align: center;">Chỉnh sửa thông tin</h2>
                                            <input id="for_img" type="file" name="img" class="form-control hidden" onchange="changeImg(this)">
                                            <div class="avatar_img">
                                               
                                                <label for="for_img"> <img style="width: 300px; height: 300px; margin-left: 160px" id="avatar" src="/avatar/{{$user->img}}"></label> 
                                               
                                            </div>
                                            <div class="form-group">
                                                <b>Họ tên</b>
                                                <input value="{{$user->full}}" type="text" name="full" class="form-control">
                                                 {{-- {{ ShowErrors($errors, 'full')}} --}}
                                            </div>
                                            
                                            <div class="form-group">
                                                <b>Email</b>
                                                <input value="{{$user->email}}" type="email" name="email" class="form-control">
                                                 {{-- {{ ShowErrors($errors, 'email')}} --}}
                                            </div>
                                            <div class="form-group">
                                                <b>Mật khẩu</b>
                                                <input value="" type="password" name="password" class="form-control" >
                                                 {{-- {{ ShowErrors($errors, 'password')}} --}}
                                            </div>
                                           
                                            <div class="form-group">
                                                <b>Địa chỉ</b>
                                                <input value="{{$user->address}}" type="text" name="address" class="form-control">
                                                 {{-- {{ ShowErrors($errors, 'address')}} --}}
                                            </div>
                                            <div class="form-group">
                                                <b>Số điện thoại</b>
                                                <input value="{{$user->phone}}" type="number" name="phone" class="form-control">
                                                 {{-- {{ ShowErrors($errors, 'phone')}} --}}
                                            </div>
                                          
                                            <div class="form-group">
                                                <b>Chức vụ</b>
                                                <select name="level" class="form-control">
                                                    <option value="0">Chức vụ</option>
                                                    <option @if($user->level==1) selected @endif value="1">Quản lý</option>
                                                    <option @if($user->level==2) selected @endif value="2">Tiếp tân</option>
                                                    <option @if($user->level==3) selected @endif value="3">Nhân viên</option>
                                                    <option @if($user->level==4) selected @endif value="4">Kế toán</option>
                                                </select>

                                            </div>
                                        </div>
                                        <div style=" text-align: center; margin-bottom: 50px">
                                            <button class="btn btn-success"  type="submit">Cập nhật thông tin</button>
                                            <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                            
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    

@endsection


@section('script')

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="../src/js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
    <script src="/admin_assets/plugins/popper.js/dist/umd/popper.min.js"></script>
    <script src="/admin_assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/admin_assets/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
    <script src="/admin_assets/plugins/screenfull/dist/screenfull.js"></script>
    <script src="/admin_assets/plugins/moment/moment.js"></script>
    <script src="/admin_assets/plugins/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="/admin_assets/plugins/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js"></script>
    <script src="/admin_assets/dist/js/theme.min.js"></script>
    <script src="/admin_assets/js/calendar.js"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->

    <script>
          function changeImg(input){
                //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
                if(input.files && input.files[0]){
                    var reader = new FileReader();
                    //Sự kiện file đã được load vào website
                    reader.onload = function(e){
                        //Thay đổi đường dẫn ảnh
                        $('#avatar').attr('src',e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $(document).ready(function() {
                $('#avatar').click(function(){
                    $('#for_img').click();
                });
            });
    </script>
    
@endsection