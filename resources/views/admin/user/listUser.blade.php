@extends('admin.master.master')
@section('title', ' User')

@section('css')

    <link rel="stylesheet" href="/admin_assets/plugins/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/ionicons/dist/css/ionicons.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/icon-kit/dist/css/iconkit.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css">
    <link rel="stylesheet" href="/admin_assets/plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" href="/admin_assets/dist/css/theme.min.css">
    <script src="/admin_assets/src/js/vendor/modernizr-2.8.3.min.js"></script>
@endsection


@section('content')


    <div class="main-content">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="ik ik-calendar bg-blue"></i>
                            <div class="d-inline">
                                <h5>Thành viên</h5>
                                <span>Danh sách thành viên</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <nav class="breadcrumb-container" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="../index.html"><i class="ik ik-home"></i></a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="#">Quản lí</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Thành viên</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="widget">
                        <div class="widget-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="state">
                                    <h6>Quản lý</h6>
                                    <h2>{{$ql}}</h2>
                                </div>
                                
                            </div>
                            <small class="text-small mt-10 d-block"></small>
                        </div>
                        <div class="progress progress-sm">
                            <div class="progress-bar bg-success" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100" style="width: 10%;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="widget">
                        <div class="widget-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="state">
                                    <h6>Tiếp tân</h6>
                                    <h2>{{$tt}}</h2>
                                </div>
                                
                            </div>
                            <small class="text-small mt-10 d-block"></small>
                        </div>
                        <div class="progress progress-sm">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="31" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="widget">
                        <div class="widget-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="state">
                                    <h6>Nhân viên</h6>
                                    <h2>{{$nv}}</h2>
                                </div>
                               
                            </div>
                            <small class="text-small mt-10 d-block"></small>
                        </div>
                        <div class="progress progress-sm">
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="widget">
                        <div class="widget-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="state">
                                    <h6>Kế toán</h6>
                                    <h2>{{$kt}}</h2>
                                </div>
                               
                            </div>
                            <small class="text-small mt-10 d-block"></small>
                        </div>
                        <div class="progress progress-sm">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 10%;"></div>
                        </div>
                    </div>
                </div>

               
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">

                    <div class="panel panel-primary">

                        <div class="panel-body">
                            <div class="bootstrap-table">
                                <div class="table-responsive">
                                    @if(session('thongbao'))
                                       <div class="alert alert-success">
                                            {{ session('thongbao') }}
                                        </div>

                                    @endif
                                    <a href="/admin/user/add" class="btn btn-primary">Thêm Thành viên</a>
                                    <table class="table table-bordered" style="margin-top:20px; text-align: center;">

                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Họ tên</th>
                                                <th>Số điện thoại</th>
                                                <th>Email</th>
                                                <th>Địa chỉ</th>
                                                <th>Chức vụ</th>
                                                <th width='18%'>Tùy chọn</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($users as $item)
                                                <tr>
                                                    <th>{{$item->id}}</th>
                                                    <th>{{$item->full}}</th>
                                                    <th>{{$item->phone}}</th>
                                                    <th>{{$item->email}}</th>
                                                    <th>{{$item->address}}</th>
                                                    <th>
                                                        @if($item->level==1) Quản lí @endif
                                                        @if($item->level==2) Tiếp tân @endif
                                                        @if($item->level==3) Nhân viên @endif
                                                        

                                                    </th>
                                                    <td>
                                                        <a href="/admin/user/edit/{{$item->id}}" class="btn btn-primary"><i class="fa fa-book" aria-hidden="true"></i> Sửa</a>
                                                        <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</a>
                                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Xóa thành viên</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="/admin/user/delete/" method="POST" accept-charset="utf-8">
                                                                        <div class="modal-body">
                                                                            @csrf
                                                                            <h4>Bạn có chắc chắn muốn xóa người này</h4>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                                                                            <button type="submit" class="btn btn-danger">Xóa</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach   
                                    
                                        </tbody>
                                    </table>
                                    {{-- <div align='right'>
                                        <ul class="pagination">
                                            {{$users->links()}}

                                        </ul>
                                    </div> --}}
                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </div>
                    </div>
                    <!--/.row-->


                </div>
            </div>
        </div>
       
    </div>
    

@endsection


@section('script')

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="../src/js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
    <script src="/admin_assets/plugins/popper.js/dist/umd/popper.min.js"></script>
    <script src="/admin_assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/admin_assets/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
    <script src="/admin_assets/plugins/screenfull/dist/screenfull.js"></script>
    <script src="/admin_assets/plugins/moment/moment.js"></script>
    <script src="/admin_assets/plugins/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="/admin_assets/plugins/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js"></script>
    <script src="/admin_assets/dist/js/theme.min.js"></script>
    <script src="/admin_assets/js/calendar.js"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    
@endsection