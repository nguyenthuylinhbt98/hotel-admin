@extends('admin.master.master')
@section('title', ' Đặt phòng')
@section('css')
    <link rel="stylesheet" href="/admin_assets/plugins/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/ionicons/dist/css/ionicons.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/icon-kit/dist/css/iconkit.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css">
    <link rel="stylesheet" href="/admin_assets/plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" href="/admin_assets/dist/css/theme.min.css">
    <link rel="stylesheet" href="/user_assets/css/styles-merged.css">
    <link rel="stylesheet" href="/user_assets/css/style.min.css">
    <link rel="stylesheet" href="/user_assets/css/custom.css">
    <script src="/admin_assets/src/js/vendor/modernizr-2.8.3.min.js"></script>
@endsection
@section('content')

@if(session('thongbao'))
<div class="alert alert-success">
    {{ session('thongbao') }}
</div>
@endif
<div class="main-content">
    <div class="container-fluid">
        <h2>Đặt phòng</h2>
        <form action="/admin/dat-phong/edit/{{$book->id}}" method="post" class="probootstrap-form">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="fname">Khách hàng/Người đại diện</label>
                        <input type="text" class="form-control" id="fname" name="name" value="{{$book->name}}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="lname">Số điện thoại</label>
                        <input type="text" class="form-control" id="lname" name="phonenumber" value="{{$book->phonenumber}}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <div class="form-field">
                    <i class="icon icon-mail"></i>
                    <input type="email" class="form-control" id="email" name="email" value="{{$book->email}}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <label for="room">Loại phòng</label>
                    <div class="form-field">
                        <i class="icon icon-chevron-down"></i>
                        <select name="room" id="room" class="form-control">
                            <option value="">Chọn loại phòng</option>
                            <option @if($book->room==1) selected @endif value="1">Classic Room</option>
                            <option @if($book->room==2) selected @endif value="2">Ultra Superior Room</option>
                            <option @if($book->room==3) selected @endif value="3">Grand Deluxe Room</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="name_room">Số phòng</label>
                    <input  class="form-control" type="text" name="name_room" value="{{$book->name_room}}">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="date-arrival">Check in</label>
                        <div class="form-field">
                            <i class="icon icon-calendar2"></i>
                            <input type="text" class="form-control" id="date-arrival" name="check_in" value="{{$book->check_in}}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="date-departure">Check out</label>
                        <div class="form-field">
                            <i class="icon icon-calendar2"></i>
                            <input type="text" class="form-control" id="date-departure" name="check_out" value="{{$book->check_out}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="adults">Người lớn</label>
                        <div class="form-field">
                            <i class="icon icon-chevron-down"></i>
                            <select name="nguoi_lon" id="adults" class="form-control">
                                <option value="">Số lượng người lớn</option>
                                <option @if($book->nguoi_lon==1) selected @endif value="1">1</option>
                                <option @if($book->nguoi_lon==2) selected @endif value="2">2</option>
                                <option @if($book->nguoi_lon==3) selected @endif value="3">3</option>
                                <option @if($book->nguoi_lon==4) selected @endif value="4">4+</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="children">Trẻ em</label>
                        <div class="form-field">
                            <i class="icon icon-chevron-down"></i>
                            <select name="tre_em" id="children" class="form-control">
                                <option value="">Số lượng trẻ em đi kèm</option>
                                <option @if($book->tre_em==0) selected @endif value="0">0</option>
                                <option @if($book->tre_em==1) selected @endif value="1">1</option>
                                <option @if($book->tre_em==2) selected @endif value="2">2</option>
                                <option @if($book->tre_em==3) selected @endif value="3">3</option>
                                <option @if($book->tre_em==4) selected @endif value="4">4+</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div >
                <label>Tình trạng</label><br>
                <div class="col-md-4">
                    <input type="checkbox" @if($book->status==0) checked @endif name="status" id="checkbox2" value="0"> <b>Đang đặt</b> 
                </div>
                <div class="col-md-4">
                    <input type="checkbox" @if($book->status==1) checked @endif name="status" id="checkbox2" value="1"> <b>Trả phòng</b>
                    
                </div>
                <div class="col-md-4">
                    
                </div>
                
            </div>
            <br>
            <hr>
            <div class="form-group">
                <label for="email">Số tiền thanh toán</label>

                <div class="form-field">
                   
                    <input type="number" class="form-control" id="email" name="sum" value="{{$book->sum}}">
                </div>
            </div>
            <div class="form-group" style="text-align: center;">
                <button type="submit" class="btn btn-primary" style="padding-bottom: 25px">Xác nhận</button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>window.jQuery || document.write('<script src="../src/js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
<script src="/admin_assets/plugins/popper.js/dist/umd/popper.min.js"></script>
<script src="/admin_assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/admin_assets/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
<script src="/admin_assets/plugins/screenfull/dist/screenfull.js"></script>
<script src="/admin_assets/plugins/moment/moment.js"></script>
<script src="/admin_assets/plugins/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="/admin_assets/plugins/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js"></script>
<script src="/admin_assets/dist/js/theme.min.js"></script>
<script src="/admin_assets/js/calendar.js"></script>
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script src="/user_assets/js/scripts.min.js"></script>
<script src="/user_assets/js/main.min.js"></script>
<script src="/user_assets/js/custom.js"></script>
@endsection