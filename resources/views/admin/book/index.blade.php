@extends('admin.master.master')
@section('title', ' Đặt phòng')

@section('css')

    <link rel="stylesheet" href="/admin_assets/plugins/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/ionicons/dist/css/ionicons.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/icon-kit/dist/css/iconkit.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css">
    <link rel="stylesheet" href="/admin_assets/plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" href="/admin_assets/dist/css/theme.min.css">
    <script src="/admin_assets/src/js/vendor/modernizr-2.8.3.min.js"></script>
@endsection


   

@section('content')
    <div class="main-content">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="ik ik-calendar bg-blue"></i>
                            <div class="d-inline">
                                <h5>Đặt phòng</h5>
                                <span>Danh sách đặt phòng</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <nav class="breadcrumb-container" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="../index.html"><i class="ik ik-home"></i></a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="#">Quản lí</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Đặt phòng</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="widget">
                        <div class="widget-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="state">
                                    <h6 style="float: left;">Yêu cầu đặt phòng</h6><span style="margin-left: 30px;" class="badge badge-success">New</span>
                                    <div style="clear: both;"></div>
                                    <h2>{{sizeof($order)}}</h2>
                                </div>
                                
                            </div>
                            <small class="text-small mt-10 d-block"></small>
                        </div>
                        <div class="progress progress-sm">
                            <div class="progress-bar bg-success" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="widget">
                        <div class="widget-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="state">
                                    <h6>Xác nhận thuê phòng</h6>
                                    <h2 style="margin-top: 8px">{{sizeof($success)}}</h2>
                                </div>
                                
                            </div>
                            <small class="text-small mt-10 d-block"></small>
                        </div>
                        <div class="progress progress-sm">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="31" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="widget">
                        <div class="widget-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="state">
                                    <h6>Lượt thuê phòng</h6>
                                    <h2 style="margin-top: 8px">{{sizeof($check_out)}}</h2>
                                </div>
                               
                            </div>
                            <small class="text-small mt-10 d-block"></small>
                        </div>
                        <div class="progress progress-sm">
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                        </div>
                    </div>
                </div>

               
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">

                    <div class="panel panel-primary">

                        <div class="panel-body">
                            <div class="bootstrap-table">
                                <div class="table-responsive">
                                    @if(session('thongbao'))
                                       <div class="alert alert-success">
                                            {{ session('thongbao') }}
                                        </div>

                                    @endif
                                    <a href="/admin/dat-phong/add" class="btn btn-primary">Đặt phòng</a>
                                    
                                    <div class="card" style="margin-top: 30px;">
                                        <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="pills-timeline-tab" data-toggle="pill" href="#current-month" role="tab" aria-controls="pills-timeline" aria-selected="true">Yêu cầu đặt phòng</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab" aria-controls="pills-profile" aria-selected="false">Đã xác nhận</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month" role="tab" aria-controls="pills-setting" aria-selected="false">Trả phòng</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content" id="pills-tabContent">
                                            {{-- yêu cầu mới --}}
                                            <div class="tab-pane fade show active" id="current-month" role="tabpanel" aria-labelledby="pills-timeline-tab">
                                                <div class="card-body">
                                                    <table class="table table-bordered" style="margin-top:20px; text-align: center;">

                                                        <thead>
                                                            <tr>
                                                                <th>ID</th>
                                                                <th>Khách hàng</th>
                                                                <th>Số điện thoại</th>
                                                                <th>Email</th>
                                                                <th>Loại phòng</th>
                                                                <th>Check in</th>
                                                                <th>Check out</th>
                                                                <th width="12%">Ngày đặt</th>
                                                                <th width='13%'>Tùy chọn</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($order as $item)
                                                                <tr>
                                                                    <th>{{$item->id}}</th>
                                                                    <th>{{$item->name}}</th>
                                                                    <th>{{$item->phonenumber}}</th>
                                                                    <th>{{$item->email}}</th>
                                                                    <th>
                                                                        @if($item->room==1) Classic Room @endif
                                                                        @if($item->room==2) Ultra Superior Room @endif
                                                                        @if($item->room==3) Grand Deluxe Room @endif
                                                                        

                                                                    </th>
                                                                    <th>{{$item->check_in}}</th>
                                                                    <th>{{$item->check_out}}</th>
                                                                    <th>{{$item->created_at}}</th>
                                                                    <td>
                                                                        <a href="/admin/dat-phong/edit/{{$item->id}}" class="btn btn-primary"><i class="fa fa-book" aria-hidden="true"></i> Xác nhận</a>
                                                                        <a style="margin-top: 10px;"  href="#" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</a>
                                                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                            <div class="modal-dialog" role="document">
                                                                                <div class="modal-content">
                                                                                    <div class="modal-header">
                                                                                        <h5 class="modal-title" id="exampleModalLabel">Xóa yêu cầu đặt phòng</h5>
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                        <span aria-hidden="true">&times;</span>
                                                                                        </button>
                                                                                    </div>
                                                                                    <form action="/admin/dat-phong/del/{{$item->id}}" method="POST" accept-charset="utf-8">
                                                                                        <div class="modal-body">
                                                                                            @csrf
                                                                                            <h4>Bạn có chắc chắn muốn xóa yêu cầu này</h4>
                                                                                        </div>
                                                                                        <div class="modal-footer">
                                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                                                                                            <button type="submit" class="btn btn-danger">Xóa</button>
                                                                                        </div>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach   
                                                    
                                                        </tbody>
                                                    </table>
                                                    
                                                </div>
                                            </div>
                                            
                                            {{-- Xác nhận --}}
                                            <div class="tab-pane fade" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                                                <div class="card-body">
                                                    <table class="table table-bordered" style="margin-top:20px; text-align: center;">

                                                        <thead>
                                                            <tr>
                                                                <th>ID</th>
                                                                <th>Khách hàng</th>
                                                                <th>Số điện thoại</th>
                                                                <th>Email</th>
                                                                <th>Loại phòng</th>
                                                                <th>Số phòng</th>
                                                                <th>Check in</th>
                                                                <th>Check out</th>
                                                                <th width='18%'>Tùy chọn</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($success as $item)
                                                                <tr>
                                                                    <th>{{$item->id}}</th>
                                                                    <th>{{$item->name}}</th>
                                                                    <th>{{$item->phonenumber}}</th>
                                                                    <th>{{$item->email}}</th>
                                                                    <th>
                                                                        @if($item->room==1) Classic Room @endif
                                                                        @if($item->room==2) Ultra Superior Room @endif
                                                                        @if($item->room==3) Grand Deluxe Room @endif
                                                                        

                                                                    </th>
                                                                    <th>{{$item->name_room}}</th>
                                                                    <th>{{$item->check_in}}</th>
                                                                    <th>{{$item->check_out}}</th>
                                                                    <td>
                                                                        <a href="/admin/dat-phong/edit/{{$item->id}}" class="btn btn-primary"><i class="fa fa-book" aria-hidden="true"></i> Trả phòng</a>
                                                                        
                                                            @endforeach   
                                                    
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            
                                            {{-- Đã trả phòng --}}
                                            <div class="tab-pane fade" id="previous-month" role="tabpanel" aria-labelledby="pills-setting-tab">
                                                <div class="card-body">
                                                   <table class="table table-bordered" style="margin-top:20px; text-align: center;">

                                                        <thead>
                                                            <tr>
                                                                <th>ID</th>
                                                                <th>Khách hàng</th>
                                                                <th>Số điện thoại</th>
                                                                <th>Email</th>
                                                                <th>Loại phòng</th>
                                                                <th>Số phòng</th>
                                                                <th>Check in</th>
                                                                <th>Check out</th>
                                                                <th>Tổng thu</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($check_out as $item)
                                                                <tr>
                                                                    <th>{{$item->id}}</th>
                                                                    <th>{{$item->name}}</th>
                                                                    <th>{{$item->phonenumber}}</th>
                                                                    <th>{{$item->email}}</th>
                                                                    <th>
                                                                        @if($item->room==1) Classic Room @endif
                                                                        @if($item->room==2) Ultra Superior Room @endif
                                                                        @if($item->room==3) Grand Deluxe Room @endif
                                                                        

                                                                    </th>
                                                                    <th>{{$item->name_room}}</th>
                                                                    <th>{{$item->check_in}}</th>
                                                                    <th>{{$item->check_out}}</th>
                                                                    <th>{{$item->sum}}</th>
                                                                </td>      
                                                            @endforeach   
                                                    
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </div>
                    </div>
                    <!--/.row-->


                </div>
            </div>
        </div>
       
    </div>
    

@endsection


@section('script')

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="../src/js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
    <script src="/admin_assets/plugins/popper.js/dist/umd/popper.min.js"></script>
    <script src="/admin_assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/admin_assets/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
    <script src="/admin_assets/plugins/screenfull/dist/screenfull.js"></script>
    <script src="/admin_assets/plugins/moment/moment.js"></script>
    <script src="/admin_assets/plugins/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="/admin_assets/plugins/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js"></script>
    <script src="/admin_assets/dist/js/theme.min.js"></script>
    <script src="/admin_assets/js/calendar.js"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    
@endsection