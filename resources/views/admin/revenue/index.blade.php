@extends('admin.master.master')
@section('title', ' Doanh thu')

@section('css')

    <link rel="stylesheet" href="/admin_assets/plugins/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/ionicons/dist/css/ionicons.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/icon-kit/dist/css/iconkit.min.css">
    <link rel="stylesheet" href="/admin_assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css">
    <link rel="stylesheet" href="/admin_assets/plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" href="/admin_assets/dist/css/theme.min.css">
    <script src="/admin_assets/src/js/vendor/modernizr-2.8.3.min.js"></script>
@endsection


   

@section('content')
    @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif

    <div class="main-content">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="ik ik-file-text bg-blue"></i>
                            <div class="d-inline">
                                <h5>Doanh thu</h5>
                                <span>Thống kê doanh thu khách sạn theo từng tháng</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <nav class="breadcrumb-container" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="../index.html"><i class="ik ik-home"></i></a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="#">Quản lý</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Doanh thu</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <div class="card capnhat_doanhthu">
                       <form action="" method="post" accept-charset="utf-8">
                            @csrf
                            <div class="card-body">
                                <h5 style="text-align: center; ">Tháng {{$month_now}}/{{$year_now}}</h5><hr>
                            </div>
                           
                            <div class="card-body "> 
                                <div class="from-control hidden">
                                    <input required class="from-control" type="number" name="thang" style="border: none; text-align: center; " value="{{$month_now}}"><hr style="margin-top: 1px;">
                                </div>
                                
                               

                                <div class="from-control">
                                    <b>Doanh thu</b>
                                    <input required class="doanh_thu from-control" type="number" name="doanh_thu" style="border: none; text-align: center; " value="{{$doanhthu}}" readonly><hr style="margin-top: 1px;">
                                    
                                </div>

                                <div class="from-control">
                                    <b>Điện</b>
                                    <input required class="chi_tieu from-control" type="number" name="dien" style="border: none; text-align: center; " value="" ><hr style="margin-top: 1px;">
                                    
                                </div>

                                <div class="from-control">
                                    <b>Nước</b>
                                    <input required class="chi_tieu from-control" type="number" name="nuoc" style="border: none; text-align: center; " value="" ><hr style="margin-top: 1px;">
                                    
                                </div>

                                <div class="from-control">
                                    <b>Mạng</b>
                                    <input required class="chi_tieu from-control" type="number" name="mang" style="border: none; text-align: center; " value="" ><hr style="margin-top: 1px;">
                                    
                                </div>


                                <div class="from-control">
                                    <b>Lương nhân viên</b>
                                    <input required class="chi_tieu from-control" type="number" name="luong_nv" style="border: none; text-align: center; " value="" ><hr style="margin-top: 1px;">
                                    
                                </div>


                                <div class="from-control">
                                    <b>Mặt bằng</b>
                                    <input required class="chi_tieu from-control" type="number" name="mat_bang" style="border: none; text-align: center; " value="" ><hr style="margin-top: 1px;">
                                    
                                </div>

                                <div class="from-control">
                                    <b>Chi khác</b>
                                    <input required class="chi_tieu from-control" type="number" name="chi_khac" style="border: none; text-align: center; " value="" re ><hr style="margin-top: 1px;">
                                    
                                </div>

                                <div class="from-control">
                                    <b>Lãi</b>
                                    <input required class="loi_nhuan from-control" type="number" name="lai" style="border: none; text-align: center; " value="" readonly>
                                    
                                </div>
                                <div class="form-group" style="text-align: center; margin-top: 30px;">
                                    <button type="submit" class="btn btn-primary" style="padding-bottom: 25px">Cập nhật</button>
                                </div>
                            </div>   
                           
                       </form>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8">
                    <div class="card">
                        <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-timeline-tab" data-toggle="pill" href="#current-month" role="tab" aria-controls="pills-timeline" aria-selected="true">Biểu đồ doanh thu</a>
                            </li>

                            {{-- <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab" aria-controls="pills-profile" aria-selected="false">Biểu đồ doanh thu</a>
                            </li> --}}
                           
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="current-month" role="tabpanel" aria-labelledby="pills-timeline-tab">
                                <div class="card-body">

                                    <div class="row">
                                       
                                        <div class="col-md-12">

                                            
                                            <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

                                            <canvas id="myChart"></canvas>

                                            <p style="text-align: center; font-size: 20px;">Biểu đồ doanh thu theo từng tháng trong năm {{$year_now}} của khách sạn</p>
                                             
                                            <script>
                                                var ctx = document.getElementById('myChart').getContext('2d');
                                                var myChart = new Chart(ctx, {
                                                    type: 'bar',
                                                    data: {

                                                        labels: [1, 2,3,4,5,6,7,8,9,10,11,12],
                                                        datasets: [{
                                                            label: 'Doanh thu trong tháng',
                                                            fillColor : "rgba(48, 164, 255, 0.2)",
                                                            strokeColor : "rgba(48, 164, 255, 1)",
                                                            pointColor : "rgba(48, 164, 255, 1)",
                                                            data: {{$pointData}},
                                                            c: [
                                                                'rgba(75, 192, 192, 0.2)'
                                                                
                                                            ],
                                                            borderColor: [
                                                                
                                                                'rgba(153, 102, 255, 1)'
                                                                
                                                            ],
                                                            borderWidth: 1
                                                        }]
                                                    },
                                                    options: {
                                                        scales: {
                                                            yAxes: [{
                                                                ticks: {
                                                                    beginAtZero: true
                                                                }
                                                            }]
                                                        }
                                                    }
                                                });
                                            </script>
                                            
                                        </div>
                                        


                                        
                                    </div>
                                    
                                </div>
                            </div>
                            {{-- <div class="tab-pane fade" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <div class="card-body">
                                   
                                </div>
                            </div> --}}
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

@endsection


@section('script')

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="../src/js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
    <script src="/admin_assets/plugins/popper.js/dist/umd/popper.min.js"></script>
    <script src="/admin_assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/admin_assets/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
    <script src="/admin_assets/plugins/screenfull/dist/screenfull.js"></script>
    <script src="/admin_assets/plugins/moment/moment.js"></script>
    <script src="/admin_assets/plugins/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="/admin_assets/plugins/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js"></script>
    <script src="/admin_assets/dist/js/theme.min.js"></script>
    <script src="/admin_assets/js/calendar.js"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
            $('input').on('input', function(e){
                var row = $('.chi_tieu');
                var a = $('.doanh_thu').val();
                var doanhthu = Number(a);
                var tong_chitieu =0;

                for(let i=0; i<row.length; i++){
                    var value = row.eq(i).val();
                    tong_chitieu += Number(value);
                } 

                var loi_nhuan = doanhthu - tong_chitieu;
                var input = $('.loi_nhuan')  ;
                input.val(loi_nhuan);
            })


        </script>

       
    
@endsection