@extends('user.master.master')
@section('content')

<section class="probootstrap-slider flexslider probootstrap-inner">
    <ul class="slides">
       <li style="background-image: url(https://newsfindhotel.b-cdn.net/wp-content/uploads/2019/09/the-easy-way-to-bag-a-hotel-room-upgrade-for-no-extra-cost-according-to-a-travel-expert-780x405.jpg);" class="overlay">
          <div class="container">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="probootstrap-slider-text text-center">
                  <p><img src="img/curve_white.svg" class="seperator probootstrap-animate" alt="Free HTML5 Bootstrap Template"></p>
                  <h1 class="probootstrap-heading probootstrap-animate">Blog</h1>
                  <div class="probootstrap-animate probootstrap-sub-wrap"> Cùng bổ sung các kinh nghiệm khi đi du lịch với BKhotel</div>
                </div>
              </div>
            </div>
          </div>
        </li>
    </ul>
  </section>
  
  <section class="probootstrap-section">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="probootstrap-hentry mb30">
            <p><a href="/"><img src="img/share_1.jpg" style="width: 100%; height: 250px;" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></a></p>
            <h3 class="mt0"><a href="/"  class="hover-reverse">Chia sẻ kinh nghiệm du lịch</a></h3>
            <p class="text-mute">July 25, 2017 • <i class="icon-bubble"></i> 3 Comments</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="probootstrap-hentry mb30">
            <p><a href="/"><img src="img/share_2.jpg" style="width: 100%; height: 250px;" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></a></p>
            <h3 class="mt0"><a href="/" class="hover-reverse">Gợi ý những cách chụp ảnh đẹp khi đi du lịch</a></h3>
            <p class="text-mute">July 25, 2017 • <i class="icon-bubble"></i> 3 Comments</p>
          </div>
        </div>
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="probootstrap-hentry mb30">
            <p><a href="/"><img src="img/share_3.png" style="width: 100%; height: 250px;" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></a></p>
            <h3 class="mt0"><a href="/" class="hover-reverse">Kinh nghiệp chọn phòng khi đi du lịch</a></h3>
            <p class="text-mute">July 25, 2017 • <i class="icon-bubble"></i> 3 Comments</p>
          </div>
        </div>

        <div class="clearfix visible-lg-block visible-md-block"></div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="probootstrap-hentry mb30">
            <p><a href="/"><img src="img/share_4.jpg" style="width: 100%; height: 250px;" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></a></p>
            <h3 class="mt0"><a href="/" class="hover-reverse">Cùng bạn thân đi khắp đất nước</a></h3>
            <p class="text-mute">July 25, 2017 • <i class="icon-bubble"></i> 3 Comments</p>
          </div>
        </div>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="probootstrap-hentry mb30">
            <p><a href="/"><img src="img/share_5.jpg" style="width: 100%; height: 250px;" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></a></p>
            <h3 class="mt0"><a href="/" class="hover-reverse">Những vật dụng cần mang khi đi du lịch</a></h3>
            <p class="text-mute">July 25, 2017 • <i class="icon-bubble"></i> 3 Comments</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="probootstrap-hentry mb30">
            <p><a href="/"><img src="img/share_6.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></a></p>
            <h3 class="mt0"><a href="/" class="hover-reverse">Bạn cần trang bị nững gì khi đi leo núi</a></h3>
            <p class="text-mute">July 25, 2017 • <i class="icon-bubble"></i> 3 Comments</p>
          </div>
        </div>
        <div class="clearfix visible-lg-block visible-md-block"></div>

      </div>
    </div>
  </section>

  <section class="probootstrap-half">
    <div class="image" style="background-image: url(http://muongthanh.com/images/brands/2019/11/14/original/muong-thanh-luxury-song-han-1_1573700065.jpg);"></div>
    <div class="text">
      <div class="probootstrap-animate fadeInUp probootstrap-animated">
        <h2 class="mt0">Best 5 Star hotel</h2>
        <p><img src="img/curve_white.svg" class="seperator" alt="Free HTML5 Bootstrap Template"></p>
        <div class="row">
          <div class="col-md-6">
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>    
          </div>
          <div class="col-md-6">
            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>    
          </div>
        </div>
        <p><a href="/" class="link-with-icon white">Learn More <i class=" icon-chevron-right"></i></a></p>
      </div>
    </div>
  </section>


@endsection