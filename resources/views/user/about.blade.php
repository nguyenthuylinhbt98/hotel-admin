@extends('user.master.master')
@section('content')

<section class="probootstrap-slider flexslider probootstrap-inner">
    <ul class="slides">
       <li style="background-image: url(https://www.icehotel.com/sites/cb_icehotel/files/styles/image_column_large/public/Kaamos-Johan-Broberg.jpg?h=3c9275bd&itok=gHToh0qO);" class="overlay">
          <div class="container">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="probootstrap-slider-text text-center">
                  <p><img src="img/curve_white.svg" class="seperator probootstrap-animate" alt="Free HTML5 Bootstrap Template"></p>
                  <h1 class="probootstrap-heading probootstrap-animate">Giới thiệu về BKhotel</h1>
                  <div class="probootstrap-animate probootstrap-sub-wrap"></div>
                </div>
              </div>
            </div>
          </div>
        </li>
    </ul>
  </section>
  
  <section class="probootstrap-section">
    <div class="container">
      <div class="row mb30">
        <div class="col-md-12">
          <figure>
          <img src="https://q-cf.bstatic.com/images/hotel/max1280x900/468/46842895.jpg" alt="Free HTML5 Bootstrap Template by uicookies.com" class="img-responsive">
          </figure>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <p>Mang trong mình vẻ đẹp sang trong với những thiết kế nôi thất hiện đại, BKhotel là điểm đến lý tưởng để du khách có một kỳ nghỉ thư giãn, thoải mái cùng với dịch vụ đẳng cấp và những lựa chọn ăn uống tuyệt vời ngay tại trung tâm thành phố. Đặt phòng tại BKhotel để tận hưởng sự sang trọng và thoải mái tối đa. Dù bạn ở đây với mục đích gì, chúng tôi sẽ mang đến cho bạn một kỳ nghỉ thật sự đáng nhớ.</p>

          {{-- <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p> --}}
        </div>
        <div class="col-md-6">
          <p>Lang thang trên những con phố, bạn sẽ có dịp đắm mình trong di sản văn hóa và những điểm tham quan hấp dẫn, chợ đêm, cửa hàng, quán bar, nhà hàng và các cửa hàng thời trang. Khách sạn cũng là điểm khởi đầu lý tưởng để bạn có thể khám phá vẻ đẹp của những điểm đến hấp dẫn lân cận.</p>
        </div>
      </div>
    </div>
  </section>

  <section class="probootstrap-section probootstrap-section-dark">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <h2 class="mt0">Tại sao chọn chúng tôi?</h2>
          <p class="mb50"><img src="img/curve.svg" class="svg" alt="Free HTML5 Bootstrap Template"></p>
        </div>
        <div class="col-md-4">
          <div class="service left-icon left-icon-sm probootstrap-animate">
            <div class="icon">
              <i class="icon-check"></i>
            </div>
            <div class="text">
              <h3>1+ Million Hotel Rooms</h3>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
              <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
            </div>  
          </div>
        </div>
        <div class="col-md-4">
          <div class="service left-icon left-icon-sm probootstrap-animate">
            <div class="icon">
              <i class="icon-check"></i>
            </div>
            <div class="text">
              <h3>Food &amp; Drinks</h3>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
              <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
            </div>  
          </div>
        </div>
        <div class="col-md-4">
          <div class="service left-icon left-icon-sm probootstrap-animate">
            <div class="icon">
              <i class="icon-check"></i>
            </div>
            <div class="text">
              <h3>Airport Taxi</h3>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
              <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
            </div>  
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="probootstrap-section">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2 mb50 text-center probootstrap-animate">
          <h2 class="mt0">Thông tin bổ sung</h2>
          <p class="mb30"><img src="img/curve.svg" class="svg" alt="Free HTML5 Bootstrap Template"></p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12 probootstrap-animate">
          <h3 class="heading-with-icon"><i class="icon-heart2"></i> <span>Các loại rượu nổi tiếng</span></h3>
          <p>Bạn muốn mua rượu vang để phục vụ cho những bữa tiệc cùng bạn bè và gia đình, để biếu tặng, kinh doanh. Royal Wine sinh ra với sứ mệnh mang đến cho toàn thể quý khách hàng những nét đẹp về văn hóa rượu vang, những kiến thức bổ ích và lợi ích của rượu vang, với những sản phẩm rượu vang nhập khẩu chính hãng</p>
          <p><a href="#" class="link-with-icon">Xem thêm <i class=" icon-chevron-right"></i></a></p>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 probootstrap-animate">
          <h3 class="heading-with-icon"><i class="icon-rocket"></i> <span>Dịch vụ đa dạng</span></h3>
          <p>Bất kỳ một khách sạn 4 – 5 sao nào cũng đều có ít nhất 1 nhà hàng sang trọng, thậm chí có khách sạn có đến 2, 3 nhà hàng. Nhà hàng trong khách sạn sẽ mang đến cho khách hàng những trải nghiệm phong cách ẩm thực khác nhau: Á, Âu….</p>
          <p><a href="#" class="link-with-icon">Xem thêm <i class=" icon-chevron-right"></i></a></p>
        </div>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-4 col-sm-6 col-xs-12 probootstrap-animate">
          <h3 class="heading-with-icon"><i class="icon-image"></i> <span>Thư viện lớn</span></h3>
          <p>Khách sạn có một khu vực thư viện riêng giành cho những người yêu sách. Bạn có thể tận hưởng không gian nhẹ nhàng ấm áp, thả mình theo những cảm xúc của từng trang sách....</p>
          <p><a href="#" class="link-with-icon">Xem thêm <i class=" icon-chevron-right"></i></a></p>
        </div>
        <div class="clearfix visible-lg-block visible-md-block"></div>
        <div class="col-md-4 col-sm-6 col-xs-12 probootstrap-animate">
          <h3 class="heading-with-icon"><i class="icon-briefcase"></i> <span>Live the Blind Texts</span></h3>
          <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
          <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
        </div>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-4 col-sm-6 col-xs-12 probootstrap-animate">
          <h3 class="heading-with-icon"><i class="icon-chat"></i> <span>Behind the Word Mountains</span></h3>
          <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
          <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 probootstrap-animate">
          <h3 class="heading-with-icon"><i class="icon-colours"></i> <span>Separated They Live</span></h3>
          <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
          <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
        </div>
        <div class="clearfix visible-lg-block visible-md-block visible-sm-block"></div>
      </div>
    </div>
  </section>
  

  <section class="probootstrap-section probootstrap-section-dark">
    <div class="container">
      <div class="row mb30">
        <div class="col-md-8 col-md-offset-2 probootstrap-section-heading text-center">
          <h2>Explore our Services</h2>
          <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
          <p><img src="img/curve.svg" class="svg" alt="Free HTML5 Bootstrap Template"></p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="service left-icon probootstrap-animate">
            <div class="icon">
              <img src="img/flaticon/svg/001-building.svg" class="svg" alt="Free HTML5 Bootstrap Template by uicookies.com">
            </div>
            <div class="text">
              <h3>1+ Million Hotel Rooms</h3>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
              <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
            </div>  
          </div>
        </div>
        <div class="col-md-4">
          <div class="service left-icon probootstrap-animate">
            <div class="icon">
              <img src="img/flaticon/svg/003-restaurant.svg" class="svg" alt="Free HTML5 Bootstrap Template by uicookies.com">
            </div>
            <div class="text">
              <h3>Food &amp; Drinks</h3>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
              <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
            </div>  
          </div>
        </div>
        <div class="col-md-4">
          <div class="service left-icon probootstrap-animate">
            <div class="icon">
              <img src="img/flaticon/svg/004-parking.svg" class="svg" alt="Free HTML5 Bootstrap Template by uicookies.com">
            </div>
            <div class="text">
              <h3>Airport Taxi</h3>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
              <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
            </div>  
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="probootstrap-half">
    <div class="image" style="background-image: url(http://muongthanh.com/images/brands/2019/11/14/original/muong-thanh-luxury-song-han-1_1573700065.jpg);"></div>
    <div class="text">
      <div class="probootstrap-animate fadeInUp probootstrap-animated">
        <h2 class="mt0">Khách sạn 5 sao đẳng cấp</h2>
        <p><img src="himg/curve_white.svg" class="seperator" alt="Free HTML5 Bootstrap Template"></p>
        <div class="row">
          <div class="col-md-6">
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>    
          </div>
          <div class="col-md-6">
            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>    
          </div>
        </div>
        <p><a href="#" class="link-with-icon white">Learn More <i class=" icon-chevron-right"></i></a></p>
      </div>
    </div>
  </section>



@endsection