 <footer role="contentinfo" class="probootstrap-footer">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="probootstrap-footer-widget">
            <p class="mt40"><img src="img/logo.jpg" class="hires" width="120" height="33" alt="Free HTML5 Bootstrap Template by uicookies.com"></p>
            <p>Ngay sát trung tâm thành phố, với những dịch vụ tiên ích nhất, khách sạn hỗ trợ phương tiện di chuyển giúp khách hàng có những trải nghiệm tuyệt vời khi đến với chúng tôi.</p>
            <p><a href="/" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
          </div>
        </div>
        <div class="col-md-4">
          <div class="probootstrap-footer-widget">
            <h3>Blog</h3>
            <ul class="probootstrap-blog-list">
              <li>
                <a href="/">
                  <figure class="probootstrap-image"><img src="http://muongthanh.com/images/brands/2019/11/14/original/muong-thanh-luxury-song-han-1_1573700065.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></figure>
                  <div class="probootstrap-text">
                    <h4>River named Duden flows</h4>
                    <span class="meta">August 2, 2017</span>
                    <p>A small river named Duden flows by their place</p>
                  </div>
                </a>
              </li>
              <li>
                <a href="/">
                  <figure class="probootstrap-image"><img src="http://muongthanh.com/images/brands/2019/11/14/original/muong-thanh-luxury-song-han-1_1573700065.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></figure>
                  <div class="probootstrap-text">
                    <h4>River named Duden flows</h4>
                    <span class="meta">August 2, 2017</span>
                    <p>A small river named Duden flows by their place</p>
                  </div>
                </a>
              </li>
              <li>
                <a href="/">
                  <figure class="probootstrap-image"><img src="http://muongthanh.com/images/brands/2019/11/14/original/muong-thanh-luxury-song-han-1_1573700065.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></figure>
                  <div class="probootstrap-text">
                    <h4>River named Duden flows</h4>
                    <span class="meta">August 2, 2017</span>
                    <p>A small river named Duden flows by their place</p>
                  </div>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-md-4">
          <div class="probootstrap-footer-widget">
            <h3>Liên hệ</h3>
            <ul class="probootstrap-contact-info">
              <li><i class="icon-location2"></i> <span>198 West 21th Street, Suite 721 New York NY 10016</span></li>
              <li><i class="icon-mail"></i><span>info@domain.com</span></li>
              <li><i class="icon-phone2"></i><span>+123 456 7890</span></li>
            </ul>
            
          </div>
        </div>
      </div>
      <div class="row mt40">
        <div class="col-md-12 text-center">
          <ul class="probootstrap-footer-social">
            <li><a href=""><i class="icon-twitter"></i></a></li>
            <li><a href=""><i class="icon-facebook"></i></a></li>
            <li><a href=""><i class="icon-instagram2"></i></a></li>
          </ul>
          <p>
            <small>&copy; 2019 <a href="/admin/login" target="_blank">Quản lý khách sạn</a>. All Rights Reserved. <br> Thiết kế &amp; xây dựng website <a href="" target="_blank">Phạm Văn Bằng</a> Đồ án tốt nghiếp 20191</small>
          </p>
          
        </div>
      </div>
    </div>
     <!--Start of Tawk.to Script-->
    <script type="text/javascript">
      var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
      (function(){
      var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
      s1.async=true;
      s1.src='https://embed.tawk.to/5d7a38e8c22bdd393bb580e2/default';
      s1.charset='UTF-8';
      s1.setAttribute('crossorigin','*');
      s0.parentNode.insertBefore(s1,s0);
      })();
    </script>
    <!--End of Tawk.to Script-->
  </footer>