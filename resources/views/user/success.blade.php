@extends('user.master.master')
@section('content')

<section class="probootstrap-slider flexslider probootstrap-inner">
    <ul class="slides">
       <li style="background-image: url(https://crystalhotel.vn/uploads/product/sp_56.jpg);" class="overlay">
          <div class="container">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="probootstrap-slider-text text-center">
                  <p><img src="img/curve_white.svg" class="seperator probootstrap-animate" alt="Free HTML5 Bootstrap Template"></p>
                  <h1 class="probootstrap-heading probootstrap-animate">Đặt phòng thành công</h1>
                  <div class="probootstrap-animate probootstrap-sub-wrap"></div>
                </div>
              </div>
            </div>
          </div>
        </li>
    </ul>
  </section>
  
  <section class="probootstrap-section">
    <div class="container">
      <div class="row probootstrap-gutter40">
        <h3> Cảm ơn quý khách hàng đã sử dụng dịch vụ của chúng tôi!!!</h3>
        <h3>Bên phía khách sạn sẽ nhanh chóng liên lạc lại với quý khách hàng</h3>
    </div>
  </section>

 
@endsection