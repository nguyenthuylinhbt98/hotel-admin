@extends('user.master.master')
@section('content')

<section class="probootstrap-slider flexslider probootstrap-inner">
    <ul class="slides">
       <li style="background-image: url(http://sunsetbeach.vn/vn/upload/images/product_1533924539.jpg);" class="overlay">
          <div class="container">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="probootstrap-slider-text text-center">
                  <p><img src="img/curve_white.svg" class="seperator probootstrap-animate" alt="Free HTML5 Bootstrap Template"></p>
                  <h1 class="probootstrap-heading probootstrap-animate">Đặt phòng</h1>
                  <div class="probootstrap-animate probootstrap-sub-wrap"></div>
                </div>
              </div>
            </div>
          </div>
        </li>
    </ul>
  </section>
  
  <section class="probootstrap-section">
    <div class="container">
      <div class="row probootstrap-gutter40">
        <div class="col-md-8">
          <h2 class="mt0">Đăng kí đặt phòng</h2>
          <form action="/reservation" method="post" class="probootstrap-form">
            @csrf
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="fname">Khách hàng / Người đại diện </label>
                  <input type="text" class="form-control" id="fname" name="name" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="lname">Số điện thoại</label>
                  <input type="text" class="form-control" id="lname" name="phonenumber" required>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="email">Email</label>
              <div class="form-field">
                <i class="icon icon-mail"></i>
                <input type="email" class="form-control" id="email" name="email">
              </div>
            </div>
             <div class="form-group">
               
                    <label for="room">Loại phòng</label>
                    <div class="form-field">
                        <i class="icon icon-chevron-down"></i>
                        <select name="room" id="room" class="form-control">
                            <option value="">Chọn loại phòng</option>
                            <option value="1">Classic Room</option>
                            <option value="2">Ultra Superior Room</option>
                            <option value="3">Grand Deluxe Room</option>
                        </select>
                    </div>
                
                
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="date-arrival">Check in</label>
                  <div class="form-field">
                    <i class="icon icon-calendar2"></i>
                    <input type="text" class="form-control" id="date-arrival" name="check_in" required>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="date-departure">Check out</label>
                  <div class="form-field">
                    <i class="icon icon-calendar2"></i>
                    <input type="text" class="form-control" id="date-departure" name="check_out" required>
                  </div>
                </div>
              </div>
            </div>

            <div class="row mb30">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="adults">Người lớn</label>
                  <div class="form-field">
                    <i class="icon icon-chevron-down"></i>
                    <select name="nguoi_lon" id="adults" class="form-control">
                      <option value="">Số lượng người lớn</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4+</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="children">Trẻ em</label>
                  <div class="form-field">
                    <i class="icon icon-chevron-down"></i>
                    <select name="tre_em" id="children" class="form-control">
                      <option value="">Số lượng trẻ em đi kèm</option>
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4+</option>
                    </select>
                  </div>
                  
                </div>
              </div>
            </div>
            <div class="form-group">
              <input type="submit" class="btn btn-primary btn-lg" id="submit" name="submit" value="Gửi yêu cầu">
            </div>
          </form>
        </div>
        <div class="col-md-4">
          <h2 class="mt0">Review</h2>
          <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
          <p><a href="#" class="btn btn-primary" role="button">Send Message</a></p>
        </div>
      </div>
    </div>
  </section>

  <section class="probootstrap-half">
    <div class="image" style="background-image: url(http://muongthanh.com/images/brands/2019/11/14/original/muong-thanh-luxury-song-han-1_1573700065.jpg);"></div>
    <div class="text">
      <div class="probootstrap-animate fadeInUp probootstrap-animated">
        <h2 class="mt0">Khách sạn 5 sao đẳng cấp</h2>
        <p><img src="img/curve_white.svg" class="seperator" alt="Free HTML5 Bootstrap Template"></p>
        <div class="row">
          <div class="col-md-6">
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>    
          </div>
          <div class="col-md-6">
            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>    
          </div>
        </div>
        <p><a href="#" class="link-with-icon white">Learn More <i class=" icon-chevron-right"></i></a></p>
      </div>
    </div>
  </section>

@endsection