@extends('user.master.master')
@section('content')
<section class="probootstrap-slider flexslider">
    <ul class="slides">
        <li style="background-image: url(https://www.imglegacyhotel.com/wp-content/uploads/2018/10/legacy-hotel-23.jpg);" class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="probootstrap-slider-text text-center">
                            <p><img style="width: 100%" src="" alt=""></p>
                            <h1 class="probootstrap-heading probootstrap-animate">Chào mừng đến với BKhotel</h1>
                            <div class="probootstrap-animate probootstrap-sub-wrap"></div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li style="background-image: url(https://r-cf.bstatic.com/images/hotel/max1024x768/227/227994240.jpg);" class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="probootstrap-slider-text text-center">
                            <p><img src="img/curve_white.svg" class="seperator probootstrap-animate" alt="Free HTML5 Bootstrap Template"></p>
                            <h1 class="probootstrap-heading probootstrap-animate">Tận hưởng trải nghiệm sang trọng</h1>
                            <div class="probootstrap-animate probootstrap-sub-wrap"></div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</section>
<section class="probootstrap-cta probootstrap-light">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="probootstrap-cta-heading">Đặt phòng cho gia đình bạn  <span> &mdash; Ngay gần trung tâm thành phố.</span></h2>
                <div class="probootstrap-cta-button-wrap"><a href="/reservation" class="btn btn-primary">Đặt phòng ngay</a></div>
            </div>
        </div>
    </div>
</section>
<section class="probootstrap-section">
    <div class="container">
        <div class="row mb30">
            <div class="col-md-8 col-md-offset-2 probootstrap-section-heading text-center">
                <h2>Khám phá Dịch vụ của chúng tôi</h2>
                <p class="lead">Ngay sát trung tâm thành phố, với những dịch vụ tiên ích nhất, khách sạn hỗ trợ phương tiện di chuyển giúp khách hàng có những trải nghiệm tuyệt vời khi đến với chúng tôi.</p>
                <p><img src="img/curve.svg" class="svg" alt="Free HTML5 Bootstrap Template"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="service left-icon probootstrap-animate">
                    <div class="icon">
                        <img src="img/flaticon/svg/001-building.svg" class="svg" alt="Free HTML5 Bootstrap Template by uicookies.com">
                    </div>
                    <div class="text">
                        <h3>Số lượng phòng lớn</h3>
                        <p>Cung cấp dịch vụ phòng với số lượng phòng lớn, đa dạng phân khúc người sử dụng.</p>
                        <p><a href="/" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service left-icon probootstrap-animate">
                    <div class="icon">
                        <img src="img/flaticon/svg/003-restaurant.svg" class="svg" alt="Free HTML5 Bootstrap Template by uicookies.com">
                    </div>
                    <div class="text">
                        <h3>Ăn &amp; Uống</h3>
                        <p>Các món ăn đặc sắc, đậm chất văn hóa dân tộc, nguyên liệu thực phẩm tươi sống, đảm bảo chất lượng.</p>
                        <p><a href="/" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service left-icon probootstrap-animate">
                    <div class="icon">
                        <img src="img/flaticon/svg/004-parking.svg" class="svg" alt="Free HTML5 Bootstrap Template by uicookies.com">
                    </div>
                    <div class="text">
                        <h3>Dịch vụ xe của khách sạn</h3>
                        <p>Khách sạn hỗ trợ dịch vụ taxi đưa đón khách hàng từ các địa điểm sân bay, nhà gà, vui lòng liên hệ trước để được tận hưởng dịch vụ tốt nhất.</p>
                        <p><a href="/" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="probootstrap-section probootstrap-section-dark">
    <div class="container">
        <div class="row mb30">
            <div class="col-md-8 col-md-offset-2 probootstrap-section-heading text-center">
                <h2>Phòng nổi bật</h2>
                <p class="lead">Ngay sát trung tâm thành phố, với những dịch vụ tiên ích nhất, khách sạn hỗ trợ phương tiện di chuyển giúp khách hàng có những trải nghiệm tuyệt vời khi đến với chúng tôi.</p>
                <p><img src="img/curve.svg" class="svg" alt="Free HTML5 Bootstrap Template"></p>
            </div>
        </div>
        <div class="row probootstrap-gutter10">
            <div class="col-md-6">
                <div class="probootstrap-block-image-text">
                    <figure>
                        <a href="img/img_1.jpg" class="image-popup">
                        <img src="img/img_1.jpg" alt="Free HTML5 Bootstrap Template by uicookies.com" class="img-responsive">
                        </a>
                        <div class="actions">
                            {{-- <a href="https://vimeo.com/45830194" class="popup-vimeo"><i class="icon-play2"></i></a> --}}

                             <iframe width="574" height="370" style="margin-bottom: -18px;" src="https://www.youtube.com/embed/50MAloxJsdE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                        </div>

                    </figure>
                    <div class="text">
                        <h3><a href="/">Grand Deluxe Room</a></h3>
                        <div class="post-meta">
                            <ul>
                                <li><span class="review-rate">4.7</span> <i class="icon-star"></i> 252 Reviews</li>
                                <li><i class="icon-user2"></i> 3 Guests</li>
                            </ul>
                        </div>
                        <p>Ngay sát trung tâm thành phố, với những dịch vụ tiên ích nhất, khách sạn hỗ trợ phương tiện di chuyển giúp khách hàng có những trải nghiệm tuyệt vời khi đến với chúng tôi.</p>
                        <p><a href="/reservation" class="btn btn-primary">Đặt phòng ngay</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="probootstrap-block-image-text">
                    <figure>
                        <a href="img/img_2.jpg" class="image-popup">
                        <img src="img/img_2.jpg" alt="Free HTML5 Bootstrap Template by uicookies.com" class="img-responsive">
                        </a>
                        <div class="actions">
                           {{--  <a href="https://vimeo.com/45830194" class="popup-vimeo"><i class="icon-play2"></i></a> --}}

                           <iframe width="574" height="370" style="margin-bottom: -18px;" src="https://www.youtube.com/embed/MjtMVhywI68" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </figure>
                    <div class="text">
                        <h3><a href="/">Ultra Superior Room</a></h3>
                        <div class="post-meta">
                            <ul>
                                <li><span class="review-rate">4.7</span> <i class="icon-star"></i> 252 Reviews</li>
                                <li><i class="icon-user2"></i> 3 Guests</li>
                            </ul>
                        </div>
                        <p>Ngay sát trung tâm thành phố, với những dịch vụ tiên ích nhất, khách sạn hỗ trợ phương tiện di chuyển giúp khách hàng có những trải nghiệm tuyệt vời khi đến với chúng tôi.</p>
                        <p><a href="/reservation" class="btn btn-primary">Đặt phòng ngay</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="probootstrap-half">
    <div class="image" style="background-image: url(http://muongthanh.com/images/brands/2019/11/14/original/muong-thanh-luxury-song-han-1_1573700065.jpg);"></div>
    <div class="text">
        <div class="probootstrap-animate fadeInUp probootstrap-animated">
            <h2 class="mt0">Khách sạn 5 sao đẳng cấp</h2>
            <p><img src="img/curve_white.svg" class="seperator" alt="Free HTML5 Bootstrap Template"></p>
            <div class="row">
                <div class="col-md-6">
                    <p>Ngay sát trung tâm thành phố, với những dịch vụ tiên ích nhất, khách sạn hỗ trợ phương tiện di chuyển giúp khách hàng có những trải nghiệm tuyệt vời khi đến với chúng tôi.</p>
                </div>
                <div class="col-md-6">
                    <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                </div>
            </div>
            <p><a href="/about" class="link-with-icon white">Xem tiếp ... <i class=" icon-chevron-right"></i></a></p>
        </div>
    </div>
</section>
@endsection