@extends('user.master.master')
@section('content')

<section class="probootstrap-slider flexslider probootstrap-inner">
    <ul class="slides">
       <li style="background-image: url(http://product.hstatic.net/1000100727/product/2019_1106_bbd_nen_10_e9de9da01fb44ed0aefb6645ecbc58e1_master.jpg);" class="overlay">
          <div class="container">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="probootstrap-slider-text text-center">
                  <p><img src="img/curve_white.svg" class="seperator probootstrap-animate" alt="Free HTML5 Bootstrap Template"></p>
                  <h1 class="probootstrap-heading probootstrap-animate">Hệ thống phòng đa dạng</h1>
                  <div class="probootstrap-animate probootstrap-sub-wrap"></div>
                </div>
              </div>
            </div>
          </div>
        </li>
    </ul>
  </section>
  
  <section class="probootstrap-section">
    <div class="container">
      <div class="row">

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="probootstrap-room">
            <a href="#"><img src="https://r-cf.bstatic.com/images/hotel/max1024x768/116/116810056.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></a>
            <div class="text">
              <h3>Phòng đơn</h3>
              <p>Giá phòng từ <strong>1.200.000/Đêm</strong></p>
              <div class="post-meta mb30">
                <ul>
                  <li><span class="review-rate">4.7</span> <i class="icon-star"></i> 252 Reviews</li>
                  <li><i class="icon-user2"></i> 30 lượt khách</li>
                </ul>
              </div>
              <p><a href="/reservation" class="btn btn-primary" role="button">Đặt phòng</a></p>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="probootstrap-room">
            <a href="#"><img src="https://pix5.agoda.net/hotelimages/281/2817185/2817185_17092210120056728137.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></a>
            <div class="text">
              <h3>Phòng đôi</h3>
              <p>Giá phòng từ <strong>1.700.000/Đêm</strong></p>
              <div class="post-meta mb30">
                <ul>
                  <li><span class="review-rate">4.7</span> <i class="icon-star"></i> 252 Reviews</li>
                  <li><i class="icon-user2"></i> 3 lượt khách</li>
                </ul>
              </div>
              <p><a href="/reservation" class="btn btn-primary" role="button">Đặt phòng</a></p>
            </div>
          </div>
        </div>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="probootstrap-room">
            <a href="#"><img src="https://pix2.agoda.net/hotelimages/263/2634754/2634754_17082212200055580946.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></a>
            <div class="text">
              <h3>Phòng đặc biệt</h3>
              <p>Starting from <strong>$29.00/Night</strong></p>
              <div class="post-meta mb30">
                <ul>
                  <li><span class="review-rate">4.7</span> <i class="icon-star"></i> 252 Reviews</li>
                  <li><i class="icon-user2"></i> 3 Guests</li>
                </ul>
              </div>
              <p><a href="/reservation" class="btn btn-primary" role="button">Dặt phòng</a></p>
            </div>
          </div>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="probootstrap-room">
            <a href="#"><img src="https://media-cdn.tripadvisor.com/media/photo-s/15/65/19/ba/superior-studio-room.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></a>
            <div class="text">
              <h3>Grand Deluxe Room</h3>
              <p>Starting from <strong>$29.00/Night</strong></p>
              <div class="post-meta mb30">
                <ul>
                  <li><span class="review-rate">4.7</span> <i class="icon-star"></i> 252 Reviews</li>
                  <li><i class="icon-user2"></i> 3 Guests</li>
                </ul>
              </div>
              <p><a href="/reservation" class="btn btn-primary" role="button">Reserve now for $29.00</a></p>
            </div>
          </div>
        </div>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="probootstrap-room">
            <a href="#"><img src="https://r-ec.bstatic.com/images/hotel/max1024x768/132/132297574.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></a>
            <div class="text">
              <h3>Ultra Superior Room</h3>
              <p>Starting from <strong>$29.00/Night</strong></p>
              <div class="post-meta mb30">
                <ul>
                  <li><span class="review-rate">4.7</span> <i class="icon-star"></i> 252 Reviews</li>
                  <li><i class="icon-user2"></i> 3 Guests</li>
                </ul>
              </div>
              <p><a href="/reservation" class="btn btn-primary" role="button">Reserve now for $29.00</a></p>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="probootstrap-room">
            <a href="#"><img src="http://www.uplevo.com/blog/wp-content/uploads/2019/08/thiet-ke-noi-that-phong-ngu.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></a>
            <div class="text">
              <h3>Presidential Room</h3>
              <p>Starting from <strong>$29.00/Night</strong></p>
              <div class="post-meta mb30">
                <ul>
                  <li><span class="review-rate">4.7</span> <i class="icon-star"></i> 252 Reviews</li>
                  <li><i class="icon-user2"></i> 3 Guests</li>
                </ul>
              </div>
              <p><a href="/reservation" class="btn btn-primary" role="button">Reserve now for $29.00</a></p>
            </div>
          </div>
        </div>
        <div class="clearfix visible-sm-block"></div>

      </div>

      <div class="row">
        <div class="col-md-12 text-center">
          <h2>Why Choose Us?</h2>
          <p class="mb50"><img src="img/curve.svg" class="svg" alt="Free HTML5 Bootstrap Template"></p>
        </div>
        <div class="col-md-4">
          <div class="service left-icon left-icon-sm probootstrap-animate">
            <div class="icon">
              <i class="icon-check"></i>
            </div>
            <div class="text">
              <h3>1+ Million Hotel Rooms</h3>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
              <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
            </div>  
          </div>
        </div>
        <div class="col-md-4">
          <div class="service left-icon left-icon-sm probootstrap-animate">
            <div class="icon">
              <i class="icon-check"></i>
            </div>
            <div class="text">
              <h3>Food &amp; Drinks</h3>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
              <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
            </div>  
          </div>
        </div>
        <div class="col-md-4">
          <div class="service left-icon left-icon-sm probootstrap-animate">
            <div class="icon">
              <i class="icon-check"></i>
            </div>
            <div class="text">
              <h3>Airport Taxi</h3>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
              <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
            </div>  
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="probootstrap-section probootstrap-section-dark">
    <div class="container">
      <div class="row mb30">
        <div class="col-md-8 col-md-offset-2 probootstrap-section-heading text-center">
          <h2>Explore our Services</h2>
          <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
          <p><img src="img/curve.svg" class="svg" alt="Free HTML5 Bootstrap Template"></p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="service left-icon probootstrap-animate">
            <div class="icon">
              <img src="img/flaticon/svg/001-building.svg" class="svg" alt="Free HTML5 Bootstrap Template by uicookies.com">
            </div>
            <div class="text">
              <h3>1+ Million Hotel Rooms</h3>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
              <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
            </div>  
          </div>
        </div>
        <div class="col-md-4">
          <div class="service left-icon probootstrap-animate">
            <div class="icon">
              <img src="img/flaticon/svg/003-restaurant.svg" class="svg" alt="Free HTML5 Bootstrap Template by uicookies.com">
            </div>
            <div class="text">
              <h3>Food &amp; Drinks</h3>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
              <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
            </div>  
          </div>
        </div>
        <div class="col-md-4">
          <div class="service left-icon probootstrap-animate">
            <div class="icon">
              <img src="img/flaticon/svg/004-parking.svg" class="svg" alt="Free HTML5 Bootstrap Template by uicookies.com">
            </div>
            <div class="text">
              <h3>Airport Taxi</h3>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
              <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
            </div>  
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="probootstrap-half">
    <div class="image" style="background-image: url(http://muongthanh.com/images/brands/2019/11/14/original/muong-thanh-luxury-song-han-1_1573700065.jpg);"></div>
    <div class="text">
      <div class="probootstrap-animate fadeInUp probootstrap-animated">
        <h2 class="mt0">Khách sạn 5 sao đảng cấp</h2>
        <p><img src="img/curve_white.svg" class="seperator" alt="Free HTML5 Bootstrap Template"></p>
        <div class="row">
          <div class="col-md-6">
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>    
          </div>
          <div class="col-md-6">
            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>    
          </div>
        </div>
        <p><a href="#" class="link-with-icon white">Learn More <i class=" icon-chevron-right"></i></a></p>
      </div>
    </div>
  </section>

@endsection