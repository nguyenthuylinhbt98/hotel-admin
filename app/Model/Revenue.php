<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Revenue extends Model
{
    protected $table = 'revenue';
    protected $fillable = [
    	'thang',
        'doanh_thu',
        'dien',
        'nuoc',
        'mang',
        'luong_nv',
        'mat_bang',
        'chi_khac',
        'lai',

    ];

    public function getList(){
        return $this->orderBy('created_at', 'DESC') ->get();
    }

    public function getList1(){
        return $this->orderBy('created_at','DESC')->paginate(6);
         
    }
}
