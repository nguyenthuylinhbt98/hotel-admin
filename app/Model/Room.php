<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'room';
    protected $fillable = [
    	'so_phong',
        'img',
        'mo_ta_ngan', 
        'mo_ta_chi_tiet', 
        'gia_phong'

    ];

    public function getList(){
        return $this->orderBy('created_at', 'DESC') ->get();
    }

    public function getList1(){
        return $this->orderBy('created_at','DESC')->paginate(6);
         
    }
}
