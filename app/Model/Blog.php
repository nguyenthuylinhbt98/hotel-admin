<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blog';
    protected $fillable = [
    	'name',
        'img', 
        'mo_ta_ngan', 
        'mo_ta_chi_tiet'

    ];

    public function getList(){
        return $this->orderBy('created_at', 'DESC') ->get();
    }

    public function getList1(){
        return $this->orderBy('created_at','DESC')->paginate(6);
         
    }
}
