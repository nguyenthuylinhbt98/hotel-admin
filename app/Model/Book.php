<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'book';
    protected $fillable = [
    	'name',
        'phonenumber',
        'email',
        'name_room',
        'room',
        'nguoi_lon',
        'tre_em',
        'check_in',
        'check_out',
       	'status',
        'sum',

    ];

    public function getList(){
        return $this->orderBy('created_at', 'DESC') ->get();
    }

    public function getList1(){
        return $this->orderBy('created_at','DESC')->paginate(6);
         
    }
}
