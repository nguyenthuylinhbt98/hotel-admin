<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Room;

class indexController extends Controller
{
    function getIndex(){
        //?lấy dữ liệu=> lấy từ database
        $room= Room::all();
    	return view('user.index', compact('room')); //mang đươc dữ liệu sang giao diện
    }

    function getAbout(){
    	return view('user.about');
    }

    function getContact(){
    	return view('user.contact');
    } 

    function getRoom(){
    	return view('user.room');
    } 


    function getBlog(){
    	return view('user.blog');
    } 

    
}
