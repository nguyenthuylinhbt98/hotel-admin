<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Book;


class reservationController extends Controller
{
	private $book;
    public function __construct(){
        $this->book = new Book();

    }

    function getReservation(){
    	return view('user.reservation');
    }

    function postReservation(Request $r){
    	$data = $r->all();
        $book = $this->book->create($data);
        return view('user.success');
    }
}
