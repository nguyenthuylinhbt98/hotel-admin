<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;

class loginController extends Controller
{
    const DASHBOARD_URL = '/admin';
    
    public function getLogin(){
        if (Auth::check()) {        	
            return redirect(self::DASHBOARD_URL);           
        }
        $user =new User();
    	return view('admin.login');
    }
    public function postLogin(Request $request){ 
    	$credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect(self::DASHBOARD_URL);        
    	}else {
            return redirect()->back()->with('alert','Wrong password or email');
        }
    }

    public function logout(){
        Auth::logout();
        return redirect('/admin/login');
    }
}
