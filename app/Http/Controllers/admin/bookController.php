<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Book;
use Carbon\Carbon;

class bookController extends Controller
{
    private $book;
    public function __construct(){
        $this->book = new Book();

    }

    function index(){
        $data['order']=Book::where('name_room', null)->orderBy('id', 'ASC')->get();
        $data['success']=Book::where('name_room', '!=', 'null')->where('status', 0)->orderBy('id', 'DESC')->get();
        $data['check_out']=Book::where('status', 1)->orderBy('id', 'DESC')->get();  
    	return view('admin.book.index', $data);
    }

    function getAdd(){
    	return view('admin.book.getAdd');
    	
    }

    function postAdd(Request $r){
    	$data = $r->all();
        $book = $this->book->create($data);
        return redirect('/admin/dat-phong')->with('thongbao', 'Thành công!!!');

    }

    function getEdit($id){
        $book = Book::find($id);
        return view('admin.book.getEdit', compact('book'));
    }

    function postEdit(Request $r, $id){
    	$book = Book::find($id);
        $data = $r->all();
        $book->update($data);
        
        return redirect()->back()->with('thongbao', 'Thành công!!!');

    }

    function del($id){
        Book::destroy($id);
        return redirect()->back()->with('thongbao', 'Bạn vừa xóa một yêu cầu đặt phòng!!!!');
    }

}
