<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Blog;

class blogController extends Controller
{
    private $blog;
    public function __construct(){
        $this->blog = new Blog();

    }

    function index(){
    	$blog = Blog::all();
    	return view('admin.blog.list', compact('blog'));
    }

    function getAdd(){
    	return view('admin.blog.add');
    }

    function postAdd(Request $r){
    	$data = $r->all();
    	// dd($data);
    	if($r->hasFile('img')){
            $file = $r->img;
            $fileName = $file->getClientOriginalName();
            $file->move('baiviet/', $fileName);
            $data['img']=$fileName;
        }    	
        $blog = $this->blog->create($data);
        return redirect('/admin/bai-viet')->with('thongbao', 'Thành công!!!');

    }

    function getEdit($id){
        $blog = Blog::find($id);
        return view('admin.blog.edit', compact('blog'));
    }

    function postEdit(Request $r, $id){
    	$blog = Blog::find($id);
        $data = $r->all();
        $blog->update($data);
        
        return redirect()->back()->with('thongbao', 'Thành công!!!');

    }

    function del($id){
        Blog::destroy($id);
        return redirect()->back()->with('thongbao', 'Bạn vừa xóa một bài viết!!!!');
    }
}
