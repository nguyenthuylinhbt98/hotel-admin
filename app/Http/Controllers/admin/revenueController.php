<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\{Revenue, Book};
use Carbon\Carbon;

class revenueController extends Controller
{
    private $revenue;
    public function __construct(){
        $this->revenue = new Revenue();

    }


    public function index()
    {
        $data['month_now'] = Carbon::now()->format('m');
        $data['year_now'] = Carbon::now()->format('Y');
        $data['doanhthu']=0;
        $data['dl']= Book::whereMonth('updated_at',  $data['month_now'])
                        ->whereYear('updated_at', $data['year_now'])
                        ->where('status', 1)
                        ->get();

        foreach ($data['dl'] as $item) {
            $data['doanhthu']+=$item->sum;
        }  

        $revenue = $this->revenue->getList();
        $point=[];
        foreach ($revenue as $value) {
                    $point[] = $value->lai;
        }
        $pointData = json_encode($point);

        return view('admin.revenue.index', $data, ['pointData'=>$pointData]);

    }

    public function store(Request $request)
    {
        $data = $request->all();
        // dd($data);
        $this->revenue->create($data);
        return redirect()->back()->with('alert', 'Cập nhật doanh thu thành công');

        
        
    }

    public function edit($id)
    {
        $revenue = $this->revenue->findOrFail($id);
        return view('admin.revenue.edit', compact('revenue'));
    }


    public function update(Request $request, $id)
    {
         $validatedData = $request->validate([
            'thang'  => 'required',
            'dien'  => 'required',
            'nuoc'  => 'required',
            'mang'  => 'required',
            'matbang'  => 'required',
            'vatchat'  => 'required',
            'luongnv'  => 'required',
            'chiphikhac'  => 'required',
            'doanhthu'  => 'required',
            'laixuat'  => 'required',
        ]);
        $revenue = $this->revenue->findOrFail($id);
        $data = $request->all();
        $revenue->update($data);
        return redirect()->back()->with('alert', 'Cập nhật doanh thu thành công');
        
    }

    public function bieudo(){
        $revenue = $this->revenue->getList1();
        $label =[]; 
        $point=[];
        foreach ($revenue as $value) {
                    $label[] = $value->thang;
                    $point[] = $value->laixuat;
        }
        $labelData = json_encode($label);
        $pointData = json_encode($point);

        return view('admin.revenue.bieudo', ['labelData'=>$labelData,'pointData'=>$pointData]);
    }
}
