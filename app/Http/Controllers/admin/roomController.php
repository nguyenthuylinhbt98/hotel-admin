<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\{Room, Book};

class roomController extends Controller
{
    private $room;
    public function __construct(){
        $this->room = new Room();

    }

    function danhsach(){
        $rooms=Book::where('name_room', '!=', 'null')->where('status', 0)->get();
        
        $dem= array();
        foreach ($rooms as $item ) {
            array_push($dem, $item->name_room);
        }
    
    	return view('admin.room.danhsach', compact('dem'));
    }

    function chiTiet($room){
        $data['book']=Book::where('name_room', $room)->get();
        $data['room']=Room::where('so_phong', $room)->get()->toarray();
        return view('admin.room.chitiet', $data);

    }

    function listRoom(){
        $rooms = Room::all();
    	return view('admin.room.listRoom', compact('rooms'));
    }

    function getAddRoom(){
    	return view('admin.room.add');

    }

    function postAddRoom(Request $r){
        $data = $r->all();
        if($r->hasFile('img')){
            $file = $r->img;
            $fileName = $r->so_phong.'-'.$file->getClientOriginalName();
            $file->move('mota/', $fileName);
            $data['img']=$fileName;
        }

        $room = $this->room->create($data);
        return redirect('/admin/room/chi-tiet')->with('thongbao', 'Thành công!!!'); 

    }

    function getEditRoom($id){
        $room = Room::find($id);
    	return view('admin.room.edit', compact('room'));

    }

    function postEditRoom(Request $r, $id){
        $room = Room::find($id);
        $data = $r->all();
        if($r->hasFile('img')){
            $file = $r->img;
            $fileName = str_slug($r->so_phong).'-'.$file->getClientOriginalName();
            $file->move('mota/', $fileName);
            $data['img']=$fileName;
        }
        $room->update($data);

        return redirect()->back()->with('thongbao', 'Thành công!!!'); 

    }
}
