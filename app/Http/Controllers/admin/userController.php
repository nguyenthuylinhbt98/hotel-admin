<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hash;
use App\User;

class userController extends Controller
{
    function __construct(){
		$this->user = new User();
	}
	
	function getList(){
		$users = $this->user->getList();
		$ql=0;
		$tt=0;
		$nv=0;
		$kt=0;
		foreach ($users as $item) {
			if ($item->level == 1) {
				$ql++;
			}
			if ($item->level == 2) {
				$tt++;
			}
			if ($item->level == 3) {
				$nv++;
			}
			if ($item->level == 4) {
				$kt++;
			}
		}
		return view('admin.user.listUser', compact('users', 'ql', 'tt', 'nv', 'kt'));

	}

    function getAdd(Request $r){
		return view('admin.user.addUser');
	}

	function postAdd(Request $r){
    	$user = $this->user;
        $user->email =$r->email;

        $password = Hash::make($r->password);
        $user->password =$password;

        $user->full =$r->full;
        $user->address =$r->address;
        $user->phone =$r->phone;
        $user->level =$r->level;
        if($r->hasFile('img')){
			$file = $r->img;
			$fileName = $file->getClientOriginalExtension();
			$file->move('avatar/', $fileName);
			$user->img=$fileName;
		}else{
			$user->img='no-img.jpg';
		}


        $user->save();
		 return redirect('/admin/user')->with('thongbao', 'đã thêm thành công');
	}


	function getEdit($iduser){
		$user = $this->user->findOrFail($iduser);
		return view('admin.user.editUser', compact('user'));

	}

	function postEdit($iduser, Request $r){
		$user = $this->user->findOrFail($iduser);
		$user->email =$r->email;

		if($r->password !== null){
        	$user->password =$r->password;
		}
        $user->full =$r->full;
        $user->address =$r->address;
        $user->phone =$r->phone;
        $user->level =$r->level;
        if($r->hasFile('img')){
			$file = $r->img;
			$fileName = $file->getClientOriginalExtension();
			$file->move('avatar/', $fileName);
			$user->img=$fileName;
		}

        $user->save();
		return redirect()->back()->with('thongbao', 'đã sửa thành công');

	}

	function delete($iduser){
        User::destroy($iduser);
        return redirect()->back()->with('thongbao', 'đã xóa thành công');

    }
}
