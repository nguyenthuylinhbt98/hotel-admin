<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace'=>'user'],function()
{
    Route::get('/', 'indexController@getIndex' );
    Route::get('/about','indexController@getAbout' );
    Route::get('/contact', 'indexController@getContact');
    Route::get('/blog', 'indexController@getBlog');
    Route::get('/room', 'indexController@getRoom');


    Route::get('/reservation', 'reservationController@getReservation');
    Route::post('/reservation', 'reservationController@postReservation');


});

    Route::get('/login', 'loginController@getLogin');
    Route::post('/login', 'loginController@postlogin');



    // -------------------admin------------------------
Route::group(['prefix'=>'admin','namespace'=>'admin'], function()
{
    Route::get('/login', 'loginController@getLogin');
    Route::post('/login', 'loginController@postLogin');

    Route::get('/logout', 'loginController@logout');


	Route::get('/', 'indexController@getIndex');

    Route::group(['prefix'=>'user'], function()
    {
        Route::get('/', 'userController@getList');
        
        Route::get('/add', 'userController@getAdd');
        Route::post('/add', 'userController@postAdd');

        Route::get('/edit/{id}', 'userController@getEdit');
        Route::post('/edit/{id}', 'userController@postEdit');

        Route::post('/delete/{iduser}', 'userController@delete');
        
    });


    Route::group(['prefix'=>'doanh-thu'], function(){
        Route::get('/', 'revenueController@index');
        Route::post('/', 'revenueController@store');

        Route::get('/edit/{id}', 'revenueController@edit');
        Route::post('/edit/{id}', 'revenueController@update');

    });

    Route::group(['prefix'=>'dat-phong'], function(){
        Route::get('/', 'bookController@index');

        Route::get('/add', 'bookController@getAdd');
        Route::post('/add', 'bookController@postAdd');

        Route::get('/edit/{id}', 'bookController@getEdit');
        Route::post('/edit/{id}', 'bookController@postEdit');

        Route::post('/del/{id}', 'bookController@del');

    });

    Route::group(['prefix'=>'bai-viet'], function(){
        Route::get('/', 'blogController@index');

        Route::get('/add', 'blogController@getAdd');
        Route::post('/add', 'blogController@postAdd');

        Route::get('/edit/{id}', 'blogController@getEdit');
        Route::post('/edit/{id}', 'blogController@postEdit');

        Route::get('/del/{id}', 'blogController@del');

    });

    Route::group(['prefix'=>'room'], function(){
        Route::get('/danhsach', 'roomController@danhsach');
        Route::get('/danhsach/{room}', 'roomController@chiTiet');


        Route::group(['prefix' => 'chi-tiet'], function() {
            Route::get('/', 'roomController@listRoom');

            Route::get('/add', 'roomController@getAddRoom');
            Route::post('/add', 'roomController@postAddRoom');

            Route::get('/edit/{id}', 'roomController@getEditRoom');
            Route::post('/edit/{id}', 'roomController@postEditRoom');
        });
    });


});

